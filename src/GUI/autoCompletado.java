/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.autocomplete.ShorthandCompletion;

/**
 *
 * @author SERVER
 * clase encargada de agregar mas predicciones de texto
 */
public class autoCompletado {
      public CompletionProvider montarAutocompletado() {
        DefaultCompletionProvider provider1 = new DefaultCompletionProvider();

        provider1.addCompletion(new BasicCompletion(provider1, "si "));
        provider1.addCompletion(new BasicCompletion(provider1, "entonces "));
        provider1.addCompletion(new BasicCompletion(provider1, "sino "));
        provider1.addCompletion(new BasicCompletion(provider1, "finsi "));
        provider1.addCompletion(new BasicCompletion(provider1, "mientras "));
        provider1.addCompletion(new BasicCompletion(provider1, "finmientras "));
        provider1.addCompletion(new BasicCompletion(provider1, "repite "));
        provider1.addCompletion(new BasicCompletion(provider1, "hasta "));
        provider1.addCompletion(new BasicCompletion(provider1, "finrepite "));
        provider1.addCompletion(new BasicCompletion(provider1, "raiz2: "));
        provider1.addCompletion(new BasicCompletion(provider1, "raiz3: "));
        provider1.addCompletion(new BasicCompletion(provider1, "muestra "));
        provider1.addCompletion(new BasicCompletion(provider1, "ingresa "));
        provider1.addCompletion(new BasicCompletion(provider1, "entero: ","Conversión de tipo"));
        provider1.addCompletion(new BasicCompletion(provider1, "cadena: ", "Conversión de tipo"));
        provider1.addCompletion(new BasicCompletion(provider1, "numerico: ","Conversión de tipo"));
        provider1.addCompletion(new BasicCompletion(provider1, "cadena "));
        provider1.addCompletion(new BasicCompletion(provider1, "numerico "));
        provider1.addCompletion(new ShorthandCompletion(provider1, "repcon", "repite i=0 ; i<10 ; 1 realiza \n finrepite \n", "Agrega una estructura 'para' con una condicional"));
        provider1.addCompletion(new ShorthandCompletion(provider1, "rep", "repite i=0 hasta 10 realiza \n finrepite \n", "Agrega una estructura 'para' sin condicional"));

        return provider1;

    }  
}
