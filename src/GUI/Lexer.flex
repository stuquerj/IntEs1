package GUI;

import static Programa.Tokens.*;
%%
%class Lexer
%type Tokens

%public 
%full
%column
%line
%char
%unicode
%ignorecase

%init{
    yycolumn = 1;
    yyline = 1; 
    yychar = 0; 
%init} 

L=[a-zA-Z][a-zA-Z0-9_]*     /*REGEX PARA IDENTIFICADOR*/
D=[0-9]+                    /*REGEX PARA DIGITOS ENTEROS*/
FL=[0-9]+(\.[  |0-9]+)?     /*REGEX PARA DECIMALES*/
T=[\"]([^\"\n]|(\\\"))*[\"] /*REGEX PARA STRINGS CON COMILLA*/
espacio=[ ,\t,\r,\n]+
CM    =   "/*""/"*([^*/]|[^*]"/"|"*"[^/])*"*"*"*/" /* REGEX COMENTARIOS MULTILINEA*/
CU      =   ("//".*\r\n)|("//".*\n)|("//".*\r)      /* REGEX COMENTARIOS LINEA*/
%{
    /*variables usadas */
    public String lexeme;
    public int ini;
    public int fin;

%}
%%
si {lexeme=yytext(); ini = zzStartRead; fin = zzMarkedPos; return Reservada;}
sino {lexeme=yytext(); ini = zzStartRead; fin = zzMarkedPos; return Reservada;}
finsi {lexeme=yytext(); ini = zzStartRead; fin = zzMarkedPos; return Reservada;} 
mientras {lexeme=yytext(); ini = zzStartRead; fin = zzMarkedPos; return Reservada;}
finmientras {lexeme=yytext(); ini = zzStartRead; fin = zzMarkedPos; return Reservada;}  
entonces {lexeme=yytext();   ini = zzStartRead; fin = zzMarkedPos; return Reservada;} 
repite {lexeme=yytext(); ini = zzStartRead; fin = zzMarkedPos; return Reservada;}
hasta {lexeme=yytext(); ini = zzStartRead; fin = zzMarkedPos; return Reservada;} 
realiza {lexeme=yytext(); ini = zzStartRead; fin = zzMarkedPos; return Reservada;}
finrepite {lexeme=yytext(); ini = zzStartRead; fin = zzMarkedPos; return Reservada;}
muestra {lexeme=yytext(); ini = zzStartRead; fin = zzMarkedPos; return Reservada;} 
ingresa {lexeme=yytext(); ini = zzStartRead; fin = zzMarkedPos; return Reservada;}      
numerico {lexeme=yytext(); ini = zzStartRead; fin = zzMarkedPos; return Reservada;}
cadena {lexeme=yytext(); ini = zzStartRead; fin = zzMarkedPos; return Reservada;}

/*todo token ignorado se le puede dar un trato especial para resaltarlo, no es el caso pero se puede anadir*/
{espacio} {/*ignore*/}
"//".* {/*ignore*/}
"=" {/*ignore*/}
"==" {/*ignore*/}
">" {/*ignore*/}
"<" {/*ignore*/}
">=" {/*ignore*/}
"<=" {/*ignore*/}
"!=" {/*ignore*/}
"+" {/*ignore*/}
"-" {/*ignore*/}
"*" {/*ignore*/}
"/" {/*ignore*/}
"(" {/*ignore*/}
")" {/*ignore*/}
";" {/*ignore*/}
"d%" {/*ignore*/}
"->" {/*ignore*/}
"**" {/*ignore*/}
"&" {/*ignore*/}
"raiz2" {lexeme=yytext();   ini = zzStartRead; fin = zzMarkedPos; return Reservada;}
"raiz3" {lexeme=yytext();   ini = zzStartRead; fin = zzMarkedPos; return Reservada;}
"pi" {lexeme=yytext();   ini = zzStartRead; fin = zzMarkedPos; return Reservada;}
"azar" {lexeme=yytext();   ini = zzStartRead; fin = zzMarkedPos; return Reservada;}
/*conversion*/
"entero:" {lexeme=yytext();   ini = zzStartRead; fin = zzMarkedPos; return Tipo;}
"numerico:" {lexeme=yytext();   ini = zzStartRead; fin = zzMarkedPos; return Tipo;}
"cadena:" {lexeme=yytext();   ini = zzStartRead; fin = zzMarkedPos; return Tipo;}

/*operadores logicos*/
"||" {/*ignore*/}
"&&" {/*ignore*/}
"&&" {/*ignore*/}


{L} {lexeme=yytext();ini = zzStartRead; fin = zzMarkedPos;  return Identificador;}
{CM} {lexeme=yytext();ini = zzStartRead; fin = zzMarkedPos;  return ComentarioLM;}
{CU} {lexeme=yytext();ini = zzStartRead; fin = zzMarkedPos;  return ComentarioLM;}
{T}* {lexeme=yytext();ini = zzStartRead; fin = zzMarkedPos; return PCadenaComilla;}
{FL} {/*ignore*/}
{D} {/*ignore*/}
 . {return ERROR;}