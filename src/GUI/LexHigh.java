/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.Color;
import java.awt.Component;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

/**
 *
 * @author SERVER
 * Esta clase admite cambios en el analizador lexico encargado del resaltado de palabras reservadas
 * puede ser editado para agregar mas casos y de esa forma resaltar mas opciones de texto.
 * es dependiente de la clase Lexer, ya que el analizador lexico esta generado en JFlex, editable
 * para agregar mas reglas, obviamente se debera compilar el archivo para aplicar esas reglas nuevas.
 */
public class LexHigh extends DefaultStyledDocument {
    Lexer lexer;
            Style reservada;//reservadas
            Style comillas;
            Style defaultStyle;//sin estilo
            Style coment;
            Style tipoC;
            StyleContext styleContext = new StyleContext();
            
            /*default color*/
    public LexHigh (){
            defaultStyle = styleContext.getStyle(StyleContext.DEFAULT_STYLE);
            coment = styleContext.addStyle("comennt", null);
            StyleConstants.setForeground(coment, new Color(111,156,199));
            StyleConstants.setItalic(coment, true);   
            StyleConstants.setBold(coment, true);
            comillas = styleContext.addStyle("comi", null);
            StyleConstants.setForeground(comillas,new Color (57, 162, 117));
            StyleConstants.setBold(comillas, true); 
            StyleConstants.setItalic(comillas, true);
            reservada = styleContext.addStyle("reser", null);
            StyleConstants.setForeground(reservada, new Color (230, 129, 39));
            StyleConstants.setBold(reservada, true);
            tipoC = styleContext.addStyle("tipoc", null);
            StyleConstants.setForeground(tipoC, new Color (230, 129, 39));
            StyleConstants.setItalic(tipoC, true);            
    } 
    
    public void setColorTipo(Color ct){
            tipoC = styleContext.addStyle("tipoc", null);
            StyleConstants.setForeground(coment, ct);
            StyleConstants.setItalic(tipoC, true);
    }
    
    public void setColorText(Color tx){
        defaultStyle = styleContext.getStyle(StyleContext.DEFAULT_STYLE);
        StyleConstants.setForeground(defaultStyle, tx);
    }
    
    public void setColorCom(Color cm){
            coment = styleContext.addStyle("comennt", null);
            StyleConstants.setForeground(coment, cm);
            StyleConstants.setItalic(coment, true);
            StyleConstants.setBold(coment, true);
    }
    
    public void setColorStr(Color cad){
            comillas = styleContext.addStyle("comi", null);
            StyleConstants.setForeground(comillas, cad);
            StyleConstants.setBold(comillas, true);     
            StyleConstants.setItalic(comillas, true);
    }
    
    public void setColorRes(Color res){
            reservada = styleContext.addStyle("reser", null);
            StyleConstants.setForeground(reservada, res);
            StyleConstants.setBold(reservada, true);
    }
    
    public void leng(StyledDocument doc1){
    
        StyledDocument doc = doc1;
        
        try {
            lexer = new Lexer(new ByteArrayInputStream(doc.getText(0, doc.getLength()).getBytes()));
            
            while(true){
                Tokens tokens = lexer.yylex();
                if(tokens==null){
                    return;
                }
                switch(tokens){
                    case ComentarioLM:
                        doc.setCharacterAttributes(lexer.ini, lexer.fin, coment, false);
                        doc.setCharacterAttributes(lexer.fin, lexer.fin, defaultStyle, true);                        
                        break;
                    case Reservada:
                        doc.setCharacterAttributes(lexer.ini, lexer.fin, reservada, false);
                        doc.setCharacterAttributes(lexer.fin, lexer.fin, defaultStyle, true);
                        break;
                        
                    case Tipo:
                        doc.setCharacterAttributes(lexer.ini, lexer.fin, tipoC, false);
                        doc.setCharacterAttributes(lexer.fin, lexer.fin, defaultStyle, true);
                        break;
                        
                    case Identificador:               
                            doc.setCharacterAttributes(lexer.ini, lexer.fin, defaultStyle, true);
                            break;
                           
                    case PCadenaComilla:
                        doc.setCharacterAttributes(lexer.ini, lexer.fin, comillas, false);
                        doc.setCharacterAttributes(lexer.fin, lexer.fin, defaultStyle, true);
                        break;

                       
                }
            } 
        }catch (IOException | BadLocationException ex) {  
        }
        
        
    }
}
