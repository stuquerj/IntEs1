/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author SERVER
 * clase que compila el lexer del resaltador de sintaxis, ejecutarla en caso de nuevas reglas o modificaciones
 * LAS CLASES QUE SE COMPILAN SE LES DEBE AJUSTAR EL PATH AL EQUIPO DONDE SE DESARROLLE
 */
public class MainLex {
    public static void main(String[] args) throws Exception {
       String ruta= "C:/Users/SERVER/Documents/NetBeansProjects/Lexhr/src/Programa/Lexer.flex";
       generar(ruta);
    }
    public static void generar(String ruta) throws IOException, Exception{
        File archivo; 
        archivo  = new File(ruta);
        JFlex.Main.generate(archivo);

     }
}
