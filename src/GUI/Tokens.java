/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

/**
 *
 * @author SERVER
 * clase de enumeracion que define los tipos de caracteres resaltables en la clase LexHigh
 */
public enum Tokens {
    Reservada,
    PCadenaComilla,
    ComentarioLM,
    Tipo,
    Identificador,
    ERROR
}
