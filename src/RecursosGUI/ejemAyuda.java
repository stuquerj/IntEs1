/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecursosGUI;

/**
 *
 * @author SERVER
 */
public class ejemAyuda {
  
    private final int caso;
    private final int nclic;
    public ejemAyuda(int nCaso, int clic){
        //en dado caso se necesitara para hacer algo distinto si se da un clic se puede manejar porque
        //se envia el conteo de clics
        this.nclic = clic;
        this.caso = nCaso;
    }

    public String Ejemplos(){
        
        
            if(nclic == 1){
            switch(caso){
                                case 1: return "/*Ayuda de Inter-Es*/ \nEste apartado sirve principalmente para que el estudiante"
                        + " pueda consultar ejemplos de las distintas herramientas que ofrece el programa.\n"+
                        "Así mismo pueda establecer nexos entre las distintas funciones y como puede utilizarlas para"
                        + " resolver sus tareas y/o ejercicios asignados en clase.";
            
            case 2: return "/*Inter-Es*/ \nEs Un programa de código abierto que permite a estudiantes aprender conceptos"
                        + " básicos de la programación, así mismo busca mejorar el aprendizaje inicial del estudiante "+
                        "con el objetivo final de fortalecer las habilidades en la interpretación y resolución de problemas de"
                        + " programación.";
            case 3: return "/*Motivación*/ \nLa principal motivación de este programa se da a raíz de la necesidad de un "
                        + " programa que se ajuste a las competencias del estudiante de nivel medio, específicamente "+
                        "del estudiante de bachillerato con orientación en computación.\nDe la misma manera, una motivo por el "
                        + " que se desarrolló el programa es para que los docentes o personal de informática puedan hacer uso "
                        + "del código fuente y de esta forma puedan hacer las modificaciones de acuerdo a las guías curriculares "
                        + "pertinentes al establecimiento o al ente regulador educativo correspondiente.";    
            }
            }
            switch(caso){
        
            case 4: return "/*Comentarios en el codigo\n" +
                            "Un comentario en el codigo es una fraccion de este \n" +
                            "Este comentario no repercute ni se analiza de parte del interprete, su uso recae en meras anotaciones del usuario o estudiante\n" +
                            "el comentario puede ser multilinea o unilinea; este es un comentario multilinea\n" +
                            "*/\n" +
                            "\n" +
                            "//Este es un comentario unilinea\n" +
                            "\n" +
                            "/* Debes usar y escribir obligatoriamente dentro de los simbolos para que cuente como comentario multilinea */\n\n" +
                            "//o escribir dos diagonales para un comentario unilinea \n";
            case 5: return  "/*Tipos de datos admitidos*/\n" +
                            "/*Los tipos de datos admitidos por Inter-Es son: numerico y cadena, no obstante se puede convertir a entero pero\n" +
                            "entero no es declarable\n" +
                            "Ejemplo:*/\n" +
                            "/*Las variables 'n1' y 't1' admiten unicamente el tipo de dato asignado, si se asigna otro mostraran error.*/\n" +
                            "numerico n1 = 23\n" +
                            "/*recuerda que valor asignado o mostrado que sea texto debe encerrarse dentro de comillas*/\n" +
                            "cadena t1 = \"texto muestra\"\n" +
                            "/*En esta linea se asigna un valor erroneo para que muestre el error el interprete*/\n" +
                            "t1 = 32    /*<- si encierras el '32' dentro de comillas dobles el error se irá y el programa se ejecutara completo*/\n" +
                            "/*por ultimo se asigna un valor admitido, pero nuevo a 'n1' para sustituirlo*/\n" +
                            "n1 = 10";
            case 6: return "/*Muestra es una sentencia que permite mostrar lo que le sea indicado en pantalla*/\n" +
                        "/*El ejemplo siguiente muestra una cadena de texto que dice 'hola mundo'*/\n" +
                        "muestra \"hola mundo\"\n" +
                        "/*El ejemplo siguiente muestra un numero */\n" +
                        "muestra 23\n" +
                        "/*asi mismo se pueden operar cantidades dentro de la instruccion muestra, CUIDADO no se pueden mezclar numeros con cadenas*/\n" +
                        "muestra 23 + 10\n" +
                        "/*también se puede mostrar numeros y cadenas si se convierten, la conversión y concatenación se explica en otro inciso de ayuda.*/\n" +
                        "muestra \"Esta es una suma de dos numeros, aunque pueden ser más: \" & (cadena:10+10)";
            
            case 7: return "/*Ingresa es una sentencia que admite el ingreso de valores por teclado*/\n" +
                        "numerico n1\n" +
                        "/*se puede ingresar un valor a n1 siempre y cuando sea numerico, de esta forma solo admite el ingreso sin mensaje*/\n" +
                        "ingresa n1\n" +
                        "muestra n1\n" +
                        "/*o bien se puede ingresar con un mensaje personalizado de esta forma:*/\n" +
                        "n1 = ingresa \"digita un numero para este ejemplo\" \n" +
                        "muestra n1" ;
            case 8: return "/*operaciones simples con operadores aritméticos*/\n" +
                            "numerico n1 = 10\n" +
                            "numerico n2 = 10\n" +
                            "\n" +
                            "numerico rr\n" +
                            "/* en esta operacion se efectua primero el parentesis, el programa respeta la jerarquia de operaciones*/\n" +
                            "rr = (n1 + n2) / 2    /*=10*/\n" +
                            "muestra rr  \n" +
                            "/*por ejemplo un calculo mas complejo*/\n" +
                            "rr = 2*((n1+n2)/2) + (2**2)    /*=24*/\n" +
                            "muestra rr\n" +
                            "/*en ambos casos el resultado respeta la jerarquia de operaciones*/ ";
            /*cadenas comp*/
            case 9: return "/*operaciones simples con cadenas y concatenacion*/\n" +
                            "/*Concatenar es sinonimo de unir, en este caso la concatenacion es una operacion unicamente realizable entre cadenas de texto*/\n" +
                            "\n" +
                            "cadena t1 = \"hola mundo\"\n" +
                            "/*si se desea concatenar a la cadena en la variable 't1' se puede añadir con el operador '&' */\n" +
                            "t1 = t1 & \" en Inter-Es\"\n" +
                            "/*Nota que se indica que el valor actual de la cadena 't1' se le añade un texto adicional */\n" +
                            "muestra t1 \n" +
                            "/*Se pueden comparar cadenas con el operador '==' */\n" +
                            "\n" +
                            "si t1 == \"hola mundo en Inter-Es\" entonces\n" +
                            "muestra \"cadenas iguales!!!\"\n" +
                            "finsi\n";                
                            
                    
            case 10: 
                   return           "/*se declaran dos variables de tipo cadena y numerico para ser mostradas al final*/\n" +
                                    "cadena var1 = \"hola mundo\"\n" +
                                    "numerico var2 = 1\n" +
                                    "muestra var1\n" +
                                    "muestra var2\n";
            case 11:
                   return           "/*se declaran dos variables numericas para obtener el resultado de cada operacion aritmética admitida*/\n" +
                                    "numerico var1\n" +
                                    "numerico var2\n" +
                                    "var1 = ingresa \"escribe el numero a obtener sus resultados\"\n" +
                                    "/*En este espacio se define primero la variable y luego la funcion a trabajar"
                                    + " raiz2: en este caso*/\n" +
                                    "var2 = raiz2:var1\n" +
                                    "muestra \"la raiz cuadrada es: \" & var2 \n" + 
                                    "var2 = raiz3:var1\n" +
                                    "muestra \"la raiz cubica es: \" & var2 \n" +
                                    "var2 = azar:var1\n" +
                                    "muestra \"Valor al azar con limite: \" & var2 \n" +
                                    "/*La potencia es n1**n2, donde n1 es la base y n2 es el exponente*/\n" +                          
                                    "var2 = var1**2\n" +
                                    "muestra \"Numero ingresado al cuadrado: \" & var2 \n";
                   
            case 12:
                return              "/*La funcion 'si' admite comparaciones simples y binarias, esta es una comparacion simple*/\n"+
                                    "numerico n1\n" +
                                    "n1= ingresa \"Ingresa un numero entre 0 y 100\"\n" +
                                    "/*Si el numero ingresado es mayor o igual a 60 entonces la condicion se vuelve verdadera:*/\n"+
                                    "si n1 >= 60 entonces\n" +
                                    "/*Como se puede apreciar, si la condición se cumple se ejecutan estas sentencias*/\n"+
                                    "muestra \"Ganaste tu asignatura\"\n" +
                                    "\n" +
                                    "sino \n" +
                                    "/*De lo contrario se ejecutan estas sentencias, por lo tanto la condicion fue falsa*/\n"+
                                    "muestra \"Perdiste tu asignatura\"\n" +
                                    "\n" +
                                    "finsi\n";
            case 13:
                return              "/*La funcion 'mientras' admite comparaciones simples y binarias, esta es una comparacion simple*/\n"+
                                    "numerico n1=0\n" +
                                    "\n" +
                                    "mientras n1<=5 entonces\n" +
                                    "muestra n1\n" +
                                    "n1 = n1+1\n" +
                                    "finmientras\n" +
                                    "\n" +
                                    "muestra \"este es el ciclo repite\"\n" +
                                    "/*La función 'repite' tiene dos formas, en esta forma se define un contador 'x' y hasta que numero contara*/\n"+
                                    "repite x1=0 hasta 5 realiza\n" +
                                    "muestra x1\n" +
                                    "finrepite\n" +
                                    "\n" +
                                    "muestra \"este es el ciclo repite/para\"\n" +
                                    "/*La funcion 'repite' en su segunda forma, se define un contador'x2', una condición y un numero que representa el aumento del contador*/\n"+
                                    "repite x2=0; x2 <= 5; 1 realiza \n" +
                                    "muestra x2\n" +
                                    "finrepite\n";
            case 14:
                return              "/*Este ejemplo muestra que un ciclo mientras no inicia si la variable 'varx' no tiene un valor mayor a cero*/\n"+
                                    "numerico varx = 0 \n" +
                                    "\n" +
                                    "si varx > 0 entonces\n" +
                                    "/*La función 'mientras' se repite indefinidamente si el numero ingresado es menor a cinco*/\n"+
                                    "mientras varx <= 5 entonces\n" +
                                    "\n" +
                                    "varx = ingresa \"Ingresa un numero mayor a 5 o se repetira esta accion\"\n" +
                                    "\n" +
                                    "finmientras\n" +
                                    "finsi\n";
            case 15:
                return              "/*Al ejecutar mira el panel derecho 'variables' y veras el resultado del programa; una concatenacion simple*/\n"+
                                    "cadena tx = \"hola \"\n" +
                                    "cadena tx1 = \"mundo en Inter-Es!!!\"\n" +
                                    "cadena res\n" +
                                    "\n" +
                                    "/*La función 'concatenacion' se hace entre dos cadenas de texto o variables del tipo CADENA EXCLUSIVAMENTE*/\n"+
                                    "res = tx & tx1 \n";
            case 16:
                return              "/*La funcion 'cadena:, numerico:, entero:' permite convertir de un tipo al otro*/\n"+
                                    "cadena n1 = \"22\"\n" +
                                    "numerico n2 = 22\n" +
                                    "\n" +
                                    "numerico res\n" +
                                    "\n" +
                                    "/*En esta linea se convierte 'n1' a numerico y se le suman 10, nota que inicialmente es una variable de tipo CADENA*/\n"+
                                    "res = 10 + numerico:n1\n" +
                                    "muestra res\n" +
                                    "\n" +
                                    "muestra \"numero: \" & cadena:n2\n" +
                                    "\n" +
                                    "muestra \"Entero: \" & entero: n2\n";
            case 17:
                return              "/*La funcion 'residual' se opera con el simbolo 'd%' y tiene como objetivo devolver el residuo de una division*/\n"+
                                    "/*Gracias a esta operacion es sencillo saber si un numero es par o impar ya que "
                                     + "un numero impar devuelve un residuo diferente a cero*/\n"+
                                    "numerico n1\n" +
                                    "\n" +
                                    "ingresa n1\n" +
                                    "\n" +
                                    "si n1 d% 2 == 0 entonces\n" +
                                    "muestra \"es un numero par\"\n" +
                                    "sino\n" +
                                    "muestra \"es un numero impar\"\n" +
                                    "finsi\n";
                    
            case 18: 
                   return   "/*En este ejercicio se combinan varias funciones, como la conversion y la estructura 'repite' */\n"+
                            "cadena res\n" +
                            "numerico n1\n" +
                            "numerico tab\n" +
                            "\n" +
                            "tab = ingresa \"Escribe un numero para ver su tabla de multiplicar:\"\n" +
                            "repite i=1 ; i<12 ; 1 realiza \n" +
                            "n1 = i * tab\n" +
                            "/*Nota la estructura de la sentencia, esta debe ser respetada si quieren hacerse varias conversiones en una sola linea "
                           + " se encierra dentro de parentesis cada conversion para que asi el interprete sepa que las conversiones/concatenaciones son independientes.*/\n"+
                            "res = (entero:tab) &\"*\" & (cadena: entero: i) & \"=\" & (cadena: entero: n1)\n" +
                            "muestra res\n" +
                            "finrepite \n";
            case 19: 
                return   "/*En este ejercicio se ejemplifica la condicional binaria. AND usa el simbolo '&&' y OR '||' */\n"+
                        "numerico n1 = 156\n" +
                        "\n" +
                         "/*AND sera verdadera cuando ambas condiciones sean verdaderas */\n"+
                        "/*Nota que 'n1' tiene como valor 156, que cumple con ambas condiciones */\n"+
                        "si n1 >= 50 && n1 >= 100 entonces\n" +
                        "muestra \"Ambas condiciones se cumplieron\"\n" +
                        "finsi\n" +
                        "\n" +
                        "n1 = 50\n" +
                        "\n" +
                         "/*OR sera verdadero si al menos una de las condiciones es verdadera */\n"+
                        "/*Nota que solo una de las condiciones cumple ya que 'n1' ahora tiene valor 50.*/\n"+
                        "si n1 >= 50 || n1 >= 100 entonces\n" +
                        "muestra \"Al menos un criterio o condicion se cumplio\"\n" +
                        "finsi\n";

            }
        
    return null;
    }
}
