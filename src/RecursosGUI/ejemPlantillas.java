/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RecursosGUI;

/**
 *
 * @author SERVER
 */
public class ejemPlantillas {
    //esta clase unicamente devuelve plantillas de codigo que pueden editar el usuario
    //se pueden agregar mas de ser requerido, primero en el modelo de jlist y luego aca en el switch
    public String Plantilla(int psel){
    
    switch (psel) {
            case 0:
                return ("\n si <condicion> entonces \n <instrucciones> \n finsi ");
              
            case 1:
                return ("\n si <condicion> entonces \n <instrucciones> \n sino \n <instrucciones> \n finsi ");
              
            case 2:
                return ("\n si <condicion1> && <condicion2> entonces \n <instrucciones> \n finsi ");
               
            case 3:
                return ("\n si <condicion1> || <condicion2> entonces \n <instrucciones> \n finsi ");
               
            case 4:
                return ("\n mientras <condicion> entonces \n <instrucciones> \n finmientras ");
                
            case 5:
                return ("\n repite <variable>=<inicio de contador> hasta <limite del contador> realiza \n <instrucciones> \n finrepite ");
               
            case 6:
                return ("\n repite <variable>=<inicio de contador> ; <condicion> ; <aumento> realiza \n <instrucciones> \n finrepite ");
               
            case 7:
                return ("\n raiz2:<variable o numero a obtener raiz cuadrada>");
               
            case 8:
                return ("\n raiz3:<variable o numero a obtener raiz cuadrada>");
               
            case 9:
                return ("\n azar:<limite aleatorio>");
               
            case 10:
                return ("\n numerico Npi = pi");
               
            case 11:
                return ("\n muestra <mensaje, expresion u operación a mostrar>");
               
            case 12:
                return ("\n <variable> = ingresa <mensaje a mostrar con comillas>");
               
            case 13:
                return ("\n ingresa <variable>");
                
            case 14:
                return ("\n cadena: <variable u operacion numérica a convertir>");
               
            case 15:
                return ("\n numerico: <variable u operacion de texto a convertir>");
               
            case 16:
                return ("\n entero: <variable u operacion de texto a redondear>");
            case 17:
                return (">"); 
            case 18:
                return ("<");                
            case 19:
                return (">=");                  
            case 20:
                return ("<=");                
            case 21:
                return ("!=");
            case 22:
                return ("d%");
            case 23:
                return ("&");                
            case 24:
                return ("&&");
            case 25:
                return ("||");
            case 26:
                return ("/");   
            case 27:
                return ("+");                 
            case 28:
                return ("-"); 
            case 29:
                return ("*");    
            case 30:
                return ("==");                
        }
        return null;
    }
}
