/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semantico;

import java.util.LinkedList;
/**
 *
 * @author SERVER
 */
public class Para implements Sentencia {
    
    /*declaracion y asignacion de variable del for*/
    private final String id;
    Simbolo.Tipo tipo;//se pone de una vez para evitar pedir el tipo de dato del for, se sobre entiende que debe ser numerico
    
    private final Operacion inicio;
    private final Operacion fin;
    /*dddddddddddddddddddddddddddddddddddddddddddddd*/
    /*repite n1=0 ; n1<10 ; 1 ------ donde se debe declarar sin tipo n1, se sobre entiende que es numerico
     la condiccion es con operadores, y el numero final es el aumento, ya sea aumento de 1 en 1 o el numero deseado*/
    private final LinkedList<Sentencia> listaSentencias;
 
      public Para(String iden, Operacion ini, Operacion finale, LinkedList<Sentencia> d) {
        /*declaracion y asignacion de variable del for*/
          
        id = iden;
        inicio = ini;
        fin = finale;
        listaSentencias=d;
        tipo = Simbolo.Tipo.NUMERICO;
    }

    @Override
    public Object ejecuta(TablaS tabla) {
        /*agregar a tabla el contador con nombre variable personalizado*/
       switch(String.valueOf(tabla.varExistente(id))){
           case "false":
                tabla.add(new Simbolo(tipo,id));
                tabla.setValor(id,inicio.ejecuta(tabla));
                break;
           case "true":
                
                return "<i><b style=\"color:Red;\"> ERROR S-E: La variable \'" + id + "\' ya existe, posible comportamiento errático, declare variable con nombre distinto en el ciclo \'repite\'.</b> </i>";
            
       }
    
       String rs="";
       String muestra="";

       while((Double) tabla.getValor(id) <= (Double) fin.ejecuta(tabla) ){
            TablaS tabla1 = new TablaS();
            tabla1.addAll(tabla);
            for(Sentencia i:listaSentencias){
                if (i!=null) {
                try {
                   rs = (String)i.ejecuta(tabla1);
                   if(rs!=null && !rs.isEmpty()){
                       muestra += rs + "<br>";
                       if(rs.contains("ERROR S-E:")){return rs;}
                   }
                } catch (Exception e) {
                }
                
            }
            
            }
            tabla1.setValor(id,((Double)tabla1.getValor(id) + 1));

        }
        return muestra.substring(0,muestra.length()-4);
   
    }
    
}
