/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semantico;

/**
 *
 * @author SERVER
 */
public class Muestra implements Sentencia {
    
    private final Sentencia content;

    public Muestra(Sentencia content){
        this.content = content;
      
    }
    
    
     
    @Override
    public Object ejecuta(TablaS tabla) {
         //System.out.println(content.ejecuta(tabla).toString()); //To change body of generated methods, choose Tools | Templates.
         return content.ejecuta(tabla).toString();
    }
    
}
