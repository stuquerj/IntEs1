/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semantico;

import java.util.LinkedList;

/**
 *
 * @author SERVER
 */
public class TablaS extends LinkedList<Simbolo> {
    
    public TablaS(){
    super();
    }
    
    public String depurarT(){
        String values="";
        for (Simbolo x:this){
            
            values += x.getTipo() + " -> " + x.getId() + " -> " + x.getValor()+ "\n"  ;
            
        }
        //values = this.stream().map(s -> s.getTipo() + "  ->  " + s.getId() + "  ->  " + s.getValor() + "\n").reduce(values, String::concat);
    return values;
    }
    
    Object getValor(String iden){
        for (Simbolo s:this) {
            if(s.getId().equals(iden)){
                if(s.getValor()== null){return "";}/*retorna una cadena vacia para que el interprete no muestre 'null'*/
                else{
                return s.getValor();
                }
            }
            
        }
        //return " <b style=\"color:Red;\"> ERROR S-E: La variable \'" + iden + "\' no esta declarada </b>";
        return "<br><b style=\"color:Red;\"> ERROR S-E: Variable no definida: </b>" + "<b style=\"color:#7475FF;\">" + iden + "</b>";
    }
    String getTipoVar(String iden){
        for (Simbolo s:this) {
            if(s.getId().equals(iden)){
                return s.getTipo();
            }
        }
         return "<b style=\"color:Red;\"> ERROR S-E: La variable \'" + iden + "\' no esta declarada </b>";
        // return "nulo";
    }
    Boolean varExistente(String iden){
        return this.stream().anyMatch(s -> (s.getId().equals(iden)));
    }
    
    Object setValor(String iden, Object valor ){
        for(Simbolo sv:this){
            if(sv.getId().equals(iden)){
               
                sv.setValor(valor);
                return null;
            }
        }
        return "ERROR S-E:La variable \'" + iden + "\' no existe o no esta dentro del ambito y no es asignable ";
    }
}
