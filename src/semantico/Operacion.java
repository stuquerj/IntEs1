/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semantico;

import java.util.Objects;

/**
 *
 * @author SERVER
 */
public class Operacion implements Sentencia {
    
    public String[] lErrores = {
   /*0*/ "<b style=\"color:Red;\"> ERROR S-E: Expresión aritmética '*' no operable: </b><br>", /*ERROR BIN ARIT*/
   /*1*/ "<b style=\"color:Red;\"> ERROR S-E: Expresión aritmética '+' no operable: </b><br>", /*ERROR BIN ARIT*/
   /*2*/ "<b style=\"color:Red;\"> ERROR S-E: Expresión aritmética '-' no operable: </b><br>", /*ERROR BIN ARIT*/
   /*3*/ "<b style=\"color:Red;\"> ERROR S-E: Expresión aritmética '/' no operable: </b><br>", /*ERROR BIN ARIT*/
   /*4*/ "<b style=\"color:Red;\"> ERROR S-E: Indeterminación. No es posible dividir por cero </b>",/*ERROR CERO ARIT*/
   /*5*/ "<b style=\"color:Red;\"> ERROR S-E: Condición no válida entre tipos de datos:</b><br>",/*ERROR BIN CONDI*/
   /*6*/ "<b style=\"color:Red;\"> ERROR S-E: La función requiere un valor de tipo NUMERICO: </b></br>",/*ERROR FUNC MATE*/
   /*7*/ "<b style=\"color:Red;\"> ERROR S-E: La conversión requiere un valor de tipo: NUMERICO: </b>",/*ERROR CONVERSION*/
   /*8*/ "<b style=\"color:Red;\"> ERROR S-E: Condición no válida, los operadores '>,&lt,&lt=,>=' no admiten operandos tipo CADENA: </b><br>",/*ERROR BIN CONDI*/  
   /*9*/ "<b style=\"color:Red;\"> ERROR S-E: Condición binaria no válida: </b><br>",
   /*10*/ "<b style=\"color:Red;\"> ERROR S-E: Expresión aritmética 'd%' no operable: </b><br>" /*ERROR BIN ARIT*/
    };
    
    public static enum Tipo_operacion{
        /*ARITMETICA BINARIA*/
        SUMA,
        RESTA,
        MULTIPLICACION,
        DIVISION,
        RESIDUAL,
        NUMERICO,
        ENTERO,
        IDENTIFICADOR,
        CADENA,
        NEGATIVO,
        MAYOR_QUE,
        MENOR_QUE,
        /*mate*/
        POTENCIA,
        RAIZ2,
        RAIZ3,
        PI,
        AZAR,

        MAYORO_IQUE,
        MENORO_IQUE,
        DIFERENTE,
        IGUAL_QUE,
        COMP_CADENA,
        /*LOGICAS*/
        AND,
        OR,
        CONCATENAR,
        CONV_NUMERICO,
        CONV_ENTERO,
        CONV_CADENA
    }
    private final Tipo_operacion tipo;
   
    
    Operacion operadorDer;
    Operacion operadorIzq;
    private Object valor;

    //maneja las operaciones de comparación binaria, como aritmeticas +-*>
    public Operacion(Tipo_operacion tipo, Operacion operadorIzq, Operacion operadorDer) {
        this.tipo = tipo;
        this.operadorDer = operadorDer;
        this.operadorIzq = operadorIzq;
      
    }
    //negativos y matematicas 
    public Operacion(Tipo_operacion tipo, Operacion operadorIzq) {
        this.tipo = tipo;
        this.operadorIzq = operadorIzq;
       
    }
    //maneja las cadenas E identificadores
    public Operacion(Tipo_operacion tipo, String cad) {
        this.valor = cad;
        this.tipo = tipo;
       
    }
    //operacion constante pi
     public Operacion(Tipo_operacion tipo) {
        this.tipo = tipo;
       
    }
     //decimal
       public Operacion(Double a) {
        this.valor=a;
        this.tipo = Tipo_operacion.NUMERICO;
      
    }
       /*entero*/
       public Operacion(Integer a){
           this.valor= a;
           this.tipo = Tipo_operacion.ENTERO;
          
       }
    
       

    @Override
    public Object ejecuta(TablaS tabla) {
       
        switch(tipo){
                /**Operacion binaria*/
            case MULTIPLICACION:
            if(operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals("String") && operadorDer.ejecuta(tabla).getClass().getSimpleName().equals("String"))
            {return operadorIzq.ejecuta(tabla) + "\n"+operadorDer.ejecuta(tabla);}
            
            if(!operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()))
            {return lErrores[0] + "<b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " * "+  operadorDer.ejecuta(tabla)+"</b>";}
            
            else{
            return (Double)operadorIzq.ejecuta(tabla) * (Double) operadorDer.ejecuta(tabla); }
            
            case DIVISION:
            if(operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals("String") && operadorDer.ejecuta(tabla).getClass().getSimpleName().equals("String"))
            {return operadorIzq.ejecuta(tabla) + "\n"+operadorDer.ejecuta(tabla);}
            
            if(!operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()))
            {return lErrores[3] + "<b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " / "+  operadorDer.ejecuta(tabla)+"</b>";}
            
            if((Double) operadorDer.ejecuta(tabla) == 0) {return lErrores[4] + ": <b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " / "+  operadorDer.ejecuta(tabla)+"</b>";}
            else{
                return (Double)operadorIzq.ejecuta(tabla) / (Double) operadorDer.ejecuta(tabla);
            }
            case RESIDUAL:
            
            if(operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals("String") && operadorDer.ejecuta(tabla).getClass().getSimpleName().equals("String"))
            {return operadorIzq.ejecuta(tabla) + "\n"+operadorDer.ejecuta(tabla);}
            
            if(!operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()))
            {return lErrores[10] + "<b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " d% "+  operadorDer.ejecuta(tabla)+"</b>";}
            
            if((Double) operadorDer.ejecuta(tabla) == 0) {return lErrores[4]+ ": <b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " d% "+  operadorDer.ejecuta(tabla)+"</b>";}
            else{
                return (Double)operadorIzq.ejecuta(tabla) % (Double) operadorDer.ejecuta(tabla);
            }            
            case SUMA:
            if(operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals("String") && operadorDer.ejecuta(tabla).getClass().getSimpleName().equals("String"))
            {return operadorIzq.ejecuta(tabla) + "\n"+operadorDer.ejecuta(tabla);}
            
            if(!operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()))
            {return lErrores[1] + "<b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " + "+  operadorDer.ejecuta(tabla)+"</b>";}
            
            else{
                return (Double)operadorIzq.ejecuta(tabla) + (Double) operadorDer.ejecuta(tabla);}
            case RESTA:
            if(operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals("String") && operadorDer.ejecuta(tabla).getClass().getSimpleName().equals("String"))
            {return operadorIzq.ejecuta(tabla) + "\n"+operadorDer.ejecuta(tabla);}
            
            if(!operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()))
            {return lErrores[2] + "<b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " - "+  operadorDer.ejecuta(tabla)+"</b>";}
            
            else{
                return (Double)operadorIzq.ejecuta(tabla) - (Double) operadorDer.ejecuta(tabla);}
            case NEGATIVO:
               
             if(operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals("String")){
                return lErrores[6] + "<b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ "</b>"; 
             } 
             else{
                return (Double)operadorIzq.ejecuta(tabla) * -1;}
                /*Operacion unaria*/ 
            case IDENTIFICADOR:
                return tabla.getValor(valor.toString());
            case NUMERICO:
                return new Double (valor.toString());
            case CADENA:
                return valor.toString();
                /*Operacion comparativa*/
            case CONCATENAR:
                return (operadorIzq.ejecuta(tabla)).toString() + (operadorDer.ejecuta(tabla)).toString();
            case MAYOR_QUE:
            if(operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals("String") && operadorDer.ejecuta(tabla).getClass().getSimpleName().equals("String"))
            {return lErrores[5]+operadorIzq.ejecuta(tabla) + " > " + operadorDer.ejecuta(tabla);}
            
            if(!operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()))
            {return lErrores[8] + "<b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " > "+  operadorDer.ejecuta(tabla)+"</b>";}
            else{
            return ((Double)operadorIzq.ejecuta(tabla))>((Double)operadorDer.ejecuta(tabla)); }                
     
            case MENOR_QUE:
            if(operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals("String") && operadorDer.ejecuta(tabla).getClass().getSimpleName().equals("String"))
            {return lErrores[5]+operadorIzq.ejecuta(tabla) + " < " + operadorDer.ejecuta(tabla);}
            
            if(!operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()))
            {return lErrores[8] + "<b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " &lt "+  operadorDer.ejecuta(tabla)+"</b>";}
            else{
                return ((Double)operadorIzq.ejecuta(tabla))<((Double)operadorDer.ejecuta(tabla));}
            
            case MAYORO_IQUE:
            if(operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals("String") && operadorDer.ejecuta(tabla).getClass().getSimpleName().equals("String"))
            {return lErrores[5]+operadorIzq.ejecuta(tabla) + " >= " + operadorDer.ejecuta(tabla);}
            
            if(!operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()))
            {return lErrores[8] + "<b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " >= "+  operadorDer.ejecuta(tabla)+"</b>";}
            else{    
                return ((Double)operadorIzq.ejecuta(tabla))>=((Double)operadorDer.ejecuta(tabla));}
            
            case MENORO_IQUE:
            if(operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals("String") && operadorDer.ejecuta(tabla).getClass().getSimpleName().equals("String"))
            {return lErrores[5]+operadorIzq.ejecuta(tabla) + " <= " + operadorDer.ejecuta(tabla);} 
            
            if(!operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()))
            {return lErrores[8] + "<b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " &lt= "+  operadorDer.ejecuta(tabla)+"</b>";}
            else{    
                return ((Double)operadorIzq.ejecuta(tabla))<=((Double)operadorDer.ejecuta(tabla));}
            
            case IGUAL_QUE:
            if(operadorIzq.ejecuta(tabla).toString().contains("ERROR S-E:") || operadorIzq.ejecuta(tabla) .toString().contains("ERROR S-E:")){
             return lErrores[5] + operadorIzq.ejecuta(tabla) + " == " + operadorDer.ejecuta(tabla);
            }    
            if(!operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()))
            {return lErrores[5] + "<b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " == "+  operadorDer.ejecuta(tabla)+"</b>";}
            /*validar cadenas con '==' */
            if("String".equals(operadorIzq.ejecuta(tabla).getClass().getSimpleName())&& "String".equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()) ){
                return operadorIzq.ejecuta(tabla).toString().equals(operadorDer.ejecuta(tabla).toString());
            }               
            else{
                return Objects.equals(((Double)operadorIzq.ejecuta(tabla)), ((Double)operadorDer.ejecuta(tabla)));
                }
            case DIFERENTE:
            if(operadorIzq.ejecuta(tabla).toString().contains("ERROR S-E:") || operadorIzq.ejecuta(tabla) .toString().contains("ERROR S-E:")){
             return lErrores[5] + operadorIzq.ejecuta(tabla) + " != " + operadorDer.ejecuta(tabla);
            }            
            if(!operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()))
            {return lErrores[5] + "<b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " != "+  operadorDer.ejecuta(tabla)+"</b>";}
            
            if("String".equals(operadorIzq.ejecuta(tabla).getClass().getSimpleName())&& "String".equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()) ){
                return !operadorIzq.ejecuta(tabla).toString().equals(operadorDer.ejecuta(tabla).toString());
            }              
            else{
                return !Objects.equals(((Double)operadorIzq.ejecuta(tabla)), ((Double)operadorDer.ejecuta(tabla)));}
            
             /*validar cadenas con '->' */   
            case COMP_CADENA:/*pendiente*/
            if(!operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()))
            {return lErrores[2];}
            else{
                return Objects.equals(((String)operadorIzq.ejecuta(tabla)), ((String)operadorDer.ejecuta(tabla)));}
                /*matematicas*/
            
            case POTENCIA:
            if(operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals("String") && operadorDer.ejecuta(tabla).getClass().getSimpleName().equals("String"))
            {return operadorIzq.ejecuta(tabla) + "\n"+operadorDer.ejecuta(tabla);}
            
            if(!operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()))
            {return lErrores[0] + "<b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " ** "+  operadorDer.ejecuta(tabla)+"</b>";}
            
            else{
                return Math.pow((Double)operadorIzq.ejecuta(tabla) , (Double) operadorDer.ejecuta(tabla));}                
            case RAIZ2:
             if(operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals("String")){
                return lErrores[6] + "<b style=\"color:#FF74E7;\"> raiz2: "+operadorIzq.ejecuta(tabla)+" </b>"; 
             } 
             else{
                return Math.sqrt((Double)operadorIzq.ejecuta(tabla));}
            case RAIZ3:
             if(operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals("String")){
                return lErrores[6]+ "<b style=\"color:#FF74E7;\"> raiz3: "+operadorIzq.ejecuta(tabla)+" </b>"; 
             } 
             else{               
                return Math.cbrt((Double)operadorIzq.ejecuta(tabla)); }                     
            case PI:
                return (Double) 3.14159;                  
            case AZAR:
            if(operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals("String")){
            return lErrores[6] + "<b style=\"color:#FF74E7;\"> azar: "+operadorIzq.ejecuta(tabla)+" </b>";
            }
            else{              
            return Double.valueOf(Math.round((Double) Math.random() * (0.0 - (Double)operadorIzq.ejecuta(tabla)) + (Double)operadorIzq.ejecuta(tabla))); }                 
            /*C. COMPUESTA O LOGICAS*/
            
            case AND:
            if(!operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()))
            {return lErrores[9] + "<b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " && "+  operadorDer.ejecuta(tabla)+"</b>";}
            else{
            return (Boolean)operadorIzq.ejecuta(tabla) && (Boolean) operadorDer.ejecuta(tabla);}
            case OR:
            if(!operadorIzq.ejecuta(tabla).getClass().getSimpleName().equals(operadorDer.ejecuta(tabla).getClass().getSimpleName()))
            {return lErrores[9] + "<b style=\"color:#FF74E7;\">" + operadorIzq.ejecuta(tabla)+ " || "+  operadorDer.ejecuta(tabla)+"</b>";}
            else{
            return (Boolean)operadorIzq.ejecuta(tabla) || (Boolean) operadorDer.ejecuta(tabla);}
            /*CONVERSION TIPOS*/
            case CONV_CADENA:
            return operadorIzq.ejecuta(tabla).toString() ;
            
            case CONV_NUMERICO:
            try {
               Double.parseDouble(operadorIzq.ejecuta(tabla).toString());
               return Double.valueOf(operadorIzq.ejecuta(tabla).toString());
            } catch (NumberFormatException e) {
                return lErrores[7] + "<b style=\"color:#FF74E7;\">" + "numerico: "+  operadorIzq.ejecuta(tabla)+"</b>";
            }
            case CONV_ENTERO:
            try {
               Math.round((Double)operadorIzq.ejecuta(tabla));
               return Math.round((Double)operadorIzq.ejecuta(tabla));
            } catch (Exception e) {
                return lErrores[7] + "<b style=\"color:#FF74E7;\">" + "entero: "+  operadorIzq.ejecuta(tabla)+"</b>";
            }     
     
            
        }
       
      return null; 
    }
    
    
}
