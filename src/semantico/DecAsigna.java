/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semantico;

/**
 *
 * @author SERVER
 */
public class DecAsigna implements Sentencia{
    
    //identificador de variable string
    private final String id;
    Simbolo.Tipo tipo;
    //tipo de variable
    private final Operacion valor;
    //valor asignado a la variable
    
    public DecAsigna(Simbolo.Tipo tip, String a, Operacion val ){
        id = a;
        tipo = tip;
        valor = val;
    }

    @Override
    public Object ejecuta(TablaS tabla) {
     if(tabla.varExistente(id)==false){
        tabla.add(new Simbolo(tipo,id));
       
              switch((String)tabla.getTipoVar(id)){
                case "NUMERICO":
                    if("String".equals(valor.ejecuta(tabla).getClass().getSimpleName()))
                    { return "<b style=\"color:Red;\"> ERROR S-E: La variable \'" + id + "\' le fue asignado un valor no compatible:</b> <br>" + "<b style=\"color:#FF74E7;\">" + valor.ejecuta(tabla) + "</b>";}
                    else{
                        tabla.setValor(id,valor.ejecuta(tabla)); 
                    }
                    break;

                case "CADENA":
                    if("Double".equals(valor.ejecuta(tabla).getClass().getSimpleName()))
                    { return "<b style=\"color:Red;\"> ERROR S-E: La variable \'" + id + "\' le fue asignado un valor no compatible:</b> <br>" + "<b style=\"color:#FF74E7;\">" + valor.ejecuta(tabla) + "</b>";}
                    else{
                        tabla.setValor(id,valor.ejecuta(tabla)); 
                    }
                    break;

            } 
    
          return null;
     }   
     else{return "<b style=\"color:Red;\"> ERROR S-E: La variable \'" + id + "\' ya esta declarada y no se puede repetir. </b>";}   
       

    
    
    }
    
    
    
    
}
