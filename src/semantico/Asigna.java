/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semantico;

/**
 *
 * @author SERVER
 */
public class Asigna implements Sentencia {
    
    
    private final String id;
    private final Operacion valor;
    
    public Asigna(String x, Operacion y){
        this.id = x;
        this.valor = y;
    }

    @Override
    public Object ejecuta(TablaS tabla) {
        switch((String)tabla.getTipoVar(id)){
                case "NUMERICO":
                    if("String".equals(valor.ejecuta(tabla).getClass().getSimpleName()))
                    { return "<b style=\"color:Red;\"> ERROR S-E: La variable \'" + id + "\' le fue asignado un valor no compatible: </b><br>" + "<b style=\"color:#FF74E7;\">" + valor.ejecuta(tabla) + "</b>";}
                    else{
                        tabla.setValor(id,valor.ejecuta(tabla)); 
                    }
                    break;

                case "CADENA":                
                    if("Double".equals(valor.ejecuta(tabla).getClass().getSimpleName()))
                    { return "<b style=\"color:Red;\"> ERROR S-E: La variable \'" + id + "\' le fue asignado un valor no compatible: </b><br>" + "<b style=\"color:#FF74E7;\">" + valor.ejecuta(tabla) + "</b>";}
                    else{
                        tabla.setValor(id,valor.ejecuta(tabla)); 
                    }
                    break;
                default: return "<b style=\"color:Red;\"> ERROR S-E: La variable \'" + id + "\' no está declarada </b>";
                 
            } 
      
        //tabla.setValor(id, valor.ejecuta(tabla));
        return null;
    }
    
}
