/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semantico;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import Programa.Grafica;

/**
 *
 * @author SERVER
 */
public class Si implements Sentencia {
    
    private final Operacion condicion;
    public List<String> inss = new ArrayList<String>();
    private final LinkedList<Sentencia> listaSentencias;
    
    
    private LinkedList<Sentencia> listaSentenciasElse;
    /*utilizable si viene un if*/
    public Si(Operacion a, LinkedList<Sentencia> b) {
        condicion=a;
        listaSentencias=b;
    }
    /*utilizable si viene un if y else*/
    public Si(Operacion a, LinkedList<Sentencia> b, LinkedList<Sentencia> c) {
        condicion=a;
        listaSentencias=b;
        listaSentenciasElse=c;
    }

    @Override
    public Object ejecuta(TablaS tabla) {
       
        String rs="";
        String muestra="";
       /*este if devuelve el error correspondiente en caso la condicion no sea valida*/
        if(!"Boolean".equals(condicion.ejecuta(tabla).getClass().getSimpleName())){return condicion.ejecuta(tabla);}
        if((Boolean)condicion.ejecuta(tabla)){
        TablaS tabla1 = new TablaS();
        tabla1.addAll(tabla);
        for(Sentencia i: listaSentencias){
           
            if (i!=null) {
                try {
                   rs = (String)i.ejecuta(tabla1);
                   if(rs!=null && !rs.isEmpty()){muestra += rs + "<br>";}
                } catch (Exception e) {
                }
                
            }
            //i.ejecuta(tabla1);
        }
         
        return muestra.substring(0,muestra.length()-4);
        }
        
        
        if(listaSentenciasElse != null){
            TablaS tabla2 = new TablaS();
            tabla2.addAll(tabla);
            for(Sentencia j: listaSentenciasElse){
            
                 if (j!=null) {
                try {
                   rs = (String)j.ejecuta(tabla2);
                   if(rs!=null && !rs.isEmpty()){muestra += rs + "<br>";}
                } catch (Exception e) {
                }
                
            }
            }

        }
        return muestra.substring(0,muestra.length()-4);
        
    }
    
    
    
}
