/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semantico;

/**
 *
 * @author SERVER
 */
public class Declara implements Sentencia {
    
    
    private final String id;
    Simbolo.Tipo tipo;
    //valor de la variable
    
    public Declara(String a, Simbolo.Tipo tip ){
        id = a;
        tipo = tip;
    }
    
    @Override
    public Object ejecuta(TablaS tabla) {
        if(tabla.varExistente(id)==false){
         tabla.add(new Simbolo(tipo,id));
          return null;
     }   
     else{
      return "<html><b style=\"color:Red;\"> ERROR S-E: La variable \'" + id + "\' ya esta declarada con tipo: '"+ tabla.getTipoVar(id) + "' y no se puede repetir. </b></html>";}  
    }
    
}
