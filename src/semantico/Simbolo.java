/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semantico;

/**
 *
 * @author SERVER
 */
public class Simbolo {
    private final Tipo tipo;
    private final String id;
    private Object valor;

    public Simbolo(Tipo tipo, String id) {
        this.tipo = tipo;
        this.id = id;
    }
    
    public void setValor(Object valor) {
        this.valor = valor;
    }

    public String getId() {
        return id;
    }
    public String getTipo(){
        return tipo.toString();
    }

    public Object getValor() {
        return valor;
    }


    
    public static enum Tipo{
        NUMERICO,
        ENTERO,
        CADENA
    }
}
