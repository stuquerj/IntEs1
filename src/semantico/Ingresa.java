/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semantico;

import javax.swing.JOptionPane;

/**
 *
 * @author SERVER
 */
public class Ingresa implements Sentencia {
    
    private final String id;
    public  String value;
    public  Operacion msn;
    
    
       /*constr. para mensaje personalizado*/
      public Ingresa(String IdVar, Operacion mPantalla){
        this.id = IdVar;
        this.value = null;
        this.msn = mPantalla;
    }

    public Ingresa(String x){
        this.id = x;
        this.msn = null;
        
    } 
    @Override
    public Object ejecuta(TablaS tabla) {
       
       if(msn!= null) {
           if(msn.ejecuta(tabla).toString().contains("ERROR S-E:")){return msn.ejecuta(tabla);}
           this.value = (JOptionPane.showInputDialog( msn.ejecuta(tabla).toString() + " "));
       }
       else{
           this.value = (JOptionPane.showInputDialog("Entrada de datos en pantalla para: \'"+id +"\'"));
       }
       switch((String)tabla.getTipoVar(id)){
           case "NUMERICO":
               try {
                   tabla.setValor(id,Double.valueOf(this.value)); 
                   return (String) this.value;
                    } catch (NumberFormatException e) {
                 return "<b style=\"color:Red;\"> ERROR S-E: La variable \'" + id + "\' le fue asignado un valor no compatible: <br></b>" + "<b style=\"color:#FF74E7;\">" + value +"</b>";
                    }

              // break;

           case "CADENA":
               tabla.setValor(id,(this.value)); 
               return (String) this.value;
              // break;
               
           
       }       
       return tabla.getValor(id);
    }
    
}
