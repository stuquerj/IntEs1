/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semantico;

import java.util.LinkedList;

/**
 *
 * @author SERVER
 */
public class Mientras implements Sentencia{
    
    
    private final Operacion condicion;
    
    private final LinkedList<Sentencia> listaSentencias;
    
    public Mientras(Operacion a, LinkedList<Sentencia> b) {
        condicion=a;
        listaSentencias=b;
    }

    @Override
    public Object ejecuta(TablaS tabla) {
        int conteo=0;
        String rs="";
        String muestra="";
        /*este if devuelve el error correspondiente en caso la condicion no sea valida*/
        if(!"Boolean".equals(condicion.ejecuta(tabla).getClass().getSimpleName())){return condicion.ejecuta(tabla);}
        while((Boolean) condicion.ejecuta(tabla)){
            TablaS tabla1 = new TablaS();
            tabla1.addAll(tabla);
            for(Sentencia i: listaSentencias){
           
            if (i!=null) {
                try {
                   rs = (String)i.ejecuta(tabla1);
                   if(rs!=null && !rs.isEmpty()){
                       muestra += rs + "<br>";
                       if(rs.contains("ERROR S-E:")){return rs;}
                   }
                   
                } catch (Exception e) {
                }
                
            }
        }
            conteo++;
            if(conteo>900){break;}
        }
      return muestra.substring(0,muestra.length()-4); 
    }
    
}
