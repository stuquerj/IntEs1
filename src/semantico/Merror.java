/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semantico;

/**
 *
 * @author SERVER
 */
public class Merror implements Sentencia {
   //private final Sentencia content;

    public static enum Tipo_error{
        ERROR_SEN,
        ERROR_ASI,
        ERROR_CONDI
    }

        
        private final Tipo_error tipo;

        public Merror(Tipo_error tipo) {
        this.tipo = tipo;
        }

    @Override
    public Object ejecuta(TablaS tabla) {
        
        switch(tipo){
            /*errores que provienen de la produccion del error*/
            case ERROR_SEN:
                return "<b style=\"color:Red;\"> ERROR S-E: Instrucción no reconocida. </b>";
            case ERROR_ASI:
                return "<b style=\"color:Red;\"> ERROR S-E: Valor asignado no reconocido </b>";
            case ERROR_CONDI:
                return "<b style=\"color:Red;\"> ERROR S-E: Condición no válida. </b>";
        }
        return null;
    }
    
}
