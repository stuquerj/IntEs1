/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package semantico;

import java.util.LinkedList;
/**
 *
 * @author SERVER
 */
public class Repite implements Sentencia {
    
    /*declaracion y asignacion de variable del for*/
    private final String id;
    Simbolo.Tipo tipo;//se pone de una vez para evitar pedir el tipo de dato del for, se sobre entiende que debeser numerico
    private final Operacion condicion;
    private final Operacion inicio;
    private final Operacion fin; //aumento
    /*dddddddddddddddddddddddddddddddddddddddddddddd*/

    private final LinkedList<Sentencia> listaSentencias;


      public Repite(String iden, Operacion ini, Operacion con, Operacion au, LinkedList<Sentencia> e) {
        /*declaracion y asignacion de variable del for*/
        this.id = iden;
        this.tipo = Simbolo.Tipo.NUMERICO;
        this.inicio = ini;
        this.condicion = con;
        this.fin = au;
        listaSentencias=e;
    }

    @Override
    public Object ejecuta(TablaS tabla) {
        switch(String.valueOf(tabla.varExistente(id))){
           case "false":
                tabla.add(new Simbolo(tipo,id));
               
                if("String".equals(inicio.ejecuta(tabla).getClass().getSimpleName()) || "String".equals(fin.ejecuta(tabla).getClass().getSimpleName())){
                    return "<b style=\"color:Red;\"> ERROR S-E: La operación requiere un valor de tipo: NUMERICO, se ingresó un valor tipo: CADENA</b>";
                }
                tabla.setValor(id,inicio.ejecuta(tabla));
                break;
           case "true":
                
                return "<b style=\"color:Red;\"> ERROR S-E: La variable \'" + id + "\' ya está en uso, posible comportamiento errático, declare variable con nombre distinto en el ciclo \'repite\'.</b>";
            
       }

       String rs="";
       String muestra="";
       if(!"Boolean".equals(condicion.ejecuta(tabla).getClass().getSimpleName())){return condicion.ejecuta(tabla);}
       while((Boolean)condicion.ejecuta(tabla)){
            TablaS tabla1 = new TablaS();
            tabla1.addAll(tabla);
            for(Sentencia i:listaSentencias){
                if (i!=null) {
                try {
                   rs = (String)i.ejecuta(tabla1);
                   if(rs!=null && !rs.isEmpty()){
                       muestra += rs + "<br>";
                       if(rs.contains("ERROR S-E:")){return rs;}
                   }
                } catch (Exception e) {
                }
                
            }
            
            }
            tabla1.setValor(id,((Double)tabla1.getValor(id) + (Double)fin.ejecuta(tabla)));
        }
     
      return muestra.substring(0,muestra.length()-4);
   
    }
    
}
