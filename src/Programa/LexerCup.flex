package Programa;
import java_cup.runtime.Symbol;
%%
%class LexerCup
%type java_cup.runtime.Symbol
%public 
%cup
%full
%line
%char
%unicode
%ignorecase

%init{ 
    yyline = 1; 
    yychar = 1; 
%init} 
/*DEFINICION DE EXPRESIONES REGULARES*/
L=[a-zA-Z][a-zA-Z0-9_]*     /*REGEX IDENTIFICADOR, SIN ESPACIOS*/
D=[0-9]+                    /*REGEX DIGITOS*/
T=[\"]([^\"\n]|(\\\"))*[\"] /*REGEX DE CADENAS DE TEXTO CON COMILLAS*/
FL=[0-9]+(\.[  |0-9]+)?     /*REGEX PARA DECIMALES*/
espacio=[ ,\t,\r,\n]+       /*REGEX PARA SECUENCIAS DE ESCAPE NO ANALIZABLES*/
CM    =   "/*""/"*([^*/]|[^*]"/"|"*"[^/])*"*"*"*/" /* REGEX COMENTARIOS MULTILINEA*/
CU      =   ("//".*\r\n)|("//".*\n)|("//".*\r)      /* REGEX COMENTARIOS LINEA*/
%{

    private Symbol symbol(int type, Object value){
        return new Symbol(type, yyline, yycolumn, value);
    }
    private Symbol symbol(int type){
        return new Symbol(type,yyline,yycolumn);
    }
%}
%%
"si" {return new Symbol(sym.Si,yychar,yyline,yytext());}
"finsi" {return new Symbol(sym.Finsi,yychar,yyline,yytext());}
"sino" {return new Symbol(sym.Sino,yychar,yyline,yytext());}
"mientras" {return new Symbol(sym.Mientras,yychar,yyline,yytext());}
"entonces" {return new Symbol(sym.Entonces,yychar,yyline,yytext());}
"finmientras" {return new Symbol(sym.Finmientras,yychar,yyline,yytext());}
"repite" {return new Symbol(sym.Repite,yychar,yyline,yytext());}
"hasta" {return new Symbol(sym.Hasta,yychar,yyline,yytext());}
"finrepite" {return new Symbol(sym.Finrepite,yychar,yyline,yytext());}
"realiza" {return new Symbol(sym.Realiza,yychar,yyline,yytext());}
"numerico" {return new Symbol(sym.Tnumero,yychar,yyline,yytext());}
"cadena" {return new Symbol(sym.Cadena,yychar,yyline,yytext());}
"muestra"  {return new Symbol(sym.Muestra,yychar,yyline,yytext());}
"ingresa" {return new Symbol(sym.Ingresa,yychar,yyline,yytext());}

/*TOKENS A IGNORAR*/
{espacio} {/*ignorar*/}
"//".* {/*ignorar*/}
{CM}   {/*ignorar*/}  
{CU}   {/*ignorar*/}    

/*OPERADORES VARIOS*/
":" {return new Symbol(sym.DosPun,yychar,yyline,yytext());}
"d%" {return new Symbol(sym.Residual,yychar,yyline,yytext());}
"=" {return new Symbol(sym.Igual,yychar,yyline,yytext());}
"==" {return new Symbol(sym.Compara,yychar,yyline,yytext());}
"->" {return new Symbol(sym.CmpStr,yychar,yyline,yytext());}
">" {return new Symbol(sym.Mayor,yychar,yyline,yytext());}
"<" {return new Symbol(sym.Menor,yychar,yyline,yytext());}
">=" {return new Symbol(sym.MayorI,yychar,yyline,yytext());}
"<=" {return new Symbol(sym.MenorI,yychar,yyline,yytext());}
"!=" {return new Symbol(sym.Diferente,yychar,yyline,yytext());}
"+" {return new Symbol(sym.Suma,yychar,yyline,yytext());}
"-" {return new Symbol(sym.Resta,yychar,yyline,yytext());}
"*" {return new Symbol(sym.Multiplicacion,yychar,yyline,yytext());}
"**" {return new Symbol(sym.Potencia,yychar,yyline,yytext());}
"/" {return new Symbol(sym.Division,yychar,yyline,yytext());}
"(" {return new Symbol(sym.ParIzq,yychar,yyline,yytext());}
")" {return new Symbol(sym.ParDer,yychar,yyline,yytext());}
"&" {return new Symbol(sym.Concatenar,yychar,yyline,yytext());}



/*funciones especiales */
"raiz2" {return new Symbol(sym.Raiz2,yychar,yyline,yytext());}
"raiz3" {return new Symbol(sym.Raiz3,yychar,yyline,yytext());}
"pi" {return new Symbol(sym.Pi,yychar,yyline,yytext());}
"azar" {return new Symbol(sym.Azar,yychar,yyline,yytext());}
"entero" {return new Symbol(sym.rEntero,yychar,yyline,yytext());}
/*operadores logicos*/
"||" {return new Symbol(sym.Or,yychar,yyline,yytext());}
"&&" {return new Symbol(sym.And,yychar,yyline,yytext());}
"&&" {return new Symbol(sym.And,yychar,yyline,yytext());}
";"           {return new Symbol(sym.Pcoma,yychar,yyline,yytext());}


/*EXPRESIONES REGULARES*/
{L}                 {return new Symbol(sym.Identificador, yychar, yyline, yytext());}
{D}                 {return new Symbol(sym.Entero, yychar, yyline, yytext());}
{FL}                {return new Symbol(sym.Decimal, yychar, yyline, yytext());}
{T}                 {return new Symbol(sym.Texto, yychar, yyline, (yytext()).substring(1,yytext().length()-1));}
 . {return new Symbol(sym.lError, yychar, yyline, yytext());}
