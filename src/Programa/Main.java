/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Programa;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author SERVER
 * esta clase sirve para compilar tanto el lexer y parser del interprete, al momento de agregar o modificar reglas lexicas o 
 * sintacticas debe ser compilado nuevamente para que de esa manera se apliquen los cambios, considerando tambien
 * que debe hacerse modificacion de la clase 'Sintax' para que se ajuste a la clase 'operacion'
 */
public class Main {
    public static void main(String[] args) throws Exception {
       String ruta= "C:/Users/SERVER/Documents/NetBeansProjects/Lexhr/src/Programa/Lexer.flex";
       String ruta1= "C:/Users/SERVER/Documents/NetBeansProjects/Lexhr/src/Programa/LexerCup.flex";
       String[] rutaS = {"-parser", "Sintax","C:/Users/SERVER/Documents/NetBeansProjects/Lexhr/src/Programa/Sintax.cup"};
       generar(ruta,ruta1,rutaS);
    }
    public static void generar(String ruta,String ruta1, String[] rutaS) throws IOException, Exception{
        File archivo; 
       // archivo  = new File(ruta);
       // JFlex.Main.generate(archivo);
        archivo = new File(ruta1);
        JFlex.Main.generate(archivo);
        java_cup.Main.main(rutaS);
   
        Path rutaSym= Paths.get("C:/Users/SERVER/Documents/NetBeansProjects/Lexhr/src/Programa/sym.java");
        if (Files.exists(rutaSym)){Files.delete(rutaSym);}
        Files.move(Paths.get("C:/Users/SERVER/Documents/NetBeansProjects/Lexhr/sym.java"),
                     Paths.get("C:/Users/SERVER/Documents/NetBeansProjects/Lexhr/src/Programa/sym.java"));
        Path rutaSin= Paths.get("C:/Users/SERVER/Documents/NetBeansProjects/Lexhr/src/Programa/Sintax.java");
        if (Files.exists(rutaSin)){Files.delete(rutaSin);}
        Files.move(Paths.get("C:/Users/SERVER/Documents/NetBeansProjects/Lexhr/Sintax.java"),
                     Paths.get("C:/Users/SERVER/Documents/NetBeansProjects/Lexhr/src/Programa/Sintax.java"));

     }
}
