/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Programa;

import GUI.LexHigh;
import GUI.autoCompletado;
import RecursosGUI.LinePainter;
import RecursosGUI.TextLineNumber;
import RecursosGUI.ejemPlantillas;
import RecursosGUI.ejemAyuda;
import com.formdev.flatlaf.intellijthemes.FlatHighContrastIJTheme;
import com.formdev.flatlaf.intellijthemes.FlatLightFlatIJTheme;
import com.formdev.flatlaf.intellijthemes.FlatVuesionIJTheme;
import com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatMaterialDarkerContrastIJTheme;
import com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatNightOwlContrastIJTheme;
import com.formdev.flatlaf.intellijthemes.materialthemeuilite.FlatSolarizedLightContrastIJTheme;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import static java.awt.event.KeyEvent.VK_CONTROL;
import static java.awt.event.KeyEvent.VK_SPACE;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java_cup.runtime.Symbol;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.UndoableEditEvent;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.plaf.FontUIResource;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.CompletionProvider;
import semantico.Sentencia;
import semantico.TablaS;

/**
 *
 * @author SERVER
 */
public class Grafica extends javax.swing.JFrame {

    /**
     * Creates new form Grafica iconos cortesía de: https://www.flaticon.com/
     */
    //resultados a pantalla
    public String stringrs = "";
    UndoManager um = new UndoManager();
    
    public String pred;
    //instancias para la creacion de resaltado de busqueda
    SimpleAttributeSet resaltado = new SimpleAttributeSet();
    SimpleAttributeSet normal = new SimpleAttributeSet();

    /*1*/
    //instancia de clase ayuda
    ejemAyuda ejemplo = null;
    //herramienta que resalta linea actual, instancia.
    LinePainter lp;
    //tamaño por defecto de la letra en editor de codigo, ampliable por evento o por menu.
    public int tamLetra = 16;
    /*instancia resaltado de sintaxis*/
    LexHigh lh = null;
    /**/
    String letra = "Segoe UI Light";
    int estilo = Font.PLAIN;
    int tLetra = 16;/*variables para editar estilos de letra chooser*/
    
  
    public void cadenaPantalla(String resultados) {
        this.stringrs += resultados + "<br>" + "\n";
    }

    public final void setNumbers() {
        TextLineNumber tln = new TextLineNumber(jTextEditor);
        jTextNotRes.setContentType("text/html");
        jScrollPane3.setRowHeaderView(tln);
        jTextEditor.setFont(new Font("Consolas", Font.PLAIN, 16));
        jTextEditor.setStyledDocument(lh = new LexHigh());
    }

    public void ejecutarAst(LinkedList<Sentencia> ast) {
        String rs = "";
        String muestra = "";
        int clinea = 1;

        if (ast == null) {
            cadenaPantalla("<br><i><b style=\"color:Red;\"> --->Ejecución detenida.</b></i></html>");
            jTextNotRes.setText(this.stringrs);
            stringrs = "";
            System.gc();
            return;
        }
        TablaS ts = new TablaS();
        
        for (Sentencia sen : ast) {
           
            if (sen != null) {
                try {
                    rs = (String) sen.ejecuta(ts);

                    if (rs != null && !rs.isEmpty()) {
                        cadenaPantalla(rs);
                    }
                    if (rs != null && rs.contains("ERROR S-E")) {
                        cadenaPantalla("<i><b style=\"color:red;\"> ->Cerca de linea: " + clinea + "</b></i>");
                        cadenaPantalla("<br><i><b style=\"color:Red;\"> --->Ejecución detenida.</b></i></html>");
                        break;
                    }
                } catch (Exception e) {
                }
            }
            clinea++;
        }
        cadenaPantalla("<br><i><b style=\"color:Green;\"> --->Ejecución del programa finalizada.</b></i></html>");
        jTextvLVar.setText(ts.depurarT());
        jTextLVar.setText(ts.depurarT());
        jTextNotRes.setText(this.stringrs);
        stringrs = "";
        System.gc();

    }

    public void verSintax() {
        String obj = jTextEditor.getText();
        Sintax sin = null;
        LinkedList<Sentencia> arbolAst = null;
        if ("".equals(jTextEditor.getText())) {
            return;
        }
        cadenaPantalla("<html><b style=\"color:Green;\">Resultado:");
        try {
            sin = new Sintax(new Programa.LexerCup(new StringReader(obj)));
            sin.parse();
            cadenaPantalla(" --->No hay errores de sintaxis en el programa</b><br>");
            jTextNotRes.setText(this.stringrs);
            try {
                Symbol simbolo = sin.getS();
                cadenaPantalla("<b style=\"color:red;\">" + " ERROR S-I cerca de linea: " + (simbolo.right) + " , cerca de:  \'" + simbolo.value + "\'. " + "</b>");
                cadenaPantalla("<br><i><b style=\"color:Red;\"> --->Ejecucion detenida.</b></i></html>");
                jTextNotRes.setText(this.stringrs);

            } catch (Exception e) {
            }

        } catch (Exception e) {
           
            Symbol simbolo = sin.getS();
            cadenaPantalla("<b style=\"color:red;\">" + " ERROR S-I cerca de linea: " + (simbolo.right) + ", cerca de:  \'" + simbolo.value + "\'. " + "</b>");
            cadenaPantalla("<br><i><b style=\"color:Red;\"> --->Ejecución detenida.</b></i></html>");
            jTextNotRes.setText(this.stringrs);
        }
        this.stringrs = "";
    }

    public void ejecutarPr() {
        //si esta vacio el editor no se ejecuta el metodo
        if(jTextEditor.getText().isEmpty()){return;}
        
        String obj = jTextEditor.getText();
        Sintax sin = null;
        LinkedList<Sentencia> arbolAst = null;

        cadenaPantalla("<html><b style=\"color:Green;\">Inicio");
        try {
             /*Primer try analiza lexico-sintactico*/
            jTextNotRes.setText(this.stringrs);
            cadenaPantalla(" --->Resultados del programa:</b><br>");
            sin = new Sintax(new Programa.LexerCup(new StringReader(obj)));
            sin.parse();
            arbolAst = sin.getAST();
            /*de no haber errores se ejecutan las instrucciones*/ 
            try {
                /*segundo try, primero se evalua si hay errores 'no definidos'*/
                Symbol simbolo = sin.getS();
                /*if, hay errores no definidos y el analizador los muestra de tal manera devolviendo los parametros de este if*/
                if(simbolo.right == -1 || simbolo.value == null){
                cadenaPantalla("<b style=\"color:red;\">" + " ERROR S-I*: Instrucción no reconocida en el código, posibles caracteres no admitidos" + "</b>");
                jTextNotRes.setText(this.stringrs);
                arbolAst= null;
                }
            else{
            /*puede que haya errores definidos entonces */   /*arbolAst se deja nulo en cada error para evitar que el analizador siga la siguient fase*/     
            cadenaPantalla("<b style=\"color:red;\">" + " ERROR S-I* en linea: " + (simbolo.right) + ", cerca de:  \'" + simbolo.value + "\'. " + "</b>");
            jTextNotRes.setText(this.stringrs);
            arbolAst=null;    }
            } catch (Exception e) {
            }

        } catch (Exception e) {
            /*error no recuperable, el mismo tratamiento que con los anteriores*/
            Symbol simbolo = sin.getS();
            if(simbolo.right == -1 || simbolo.value == null){
            cadenaPantalla("<b style=\"color:red;\">" + " ERROR S-I*: Instrucción no reconocida en el código, posibles caracteres no admitidos." + "</b>");
            arbolAst=null;
            }
            else{
            cadenaPantalla("<b style=\"color:red;\">" + " ERROR S-I* en linea: " + (simbolo.right) + ", cerca de:  \'" + simbolo.value + "\'. " + "</b>");
            jTextNotRes.setText(this.stringrs);
            arbolAst=null;
            }
        } 
         ejecutarAst(arbolAst);
    }

    public boolean guardar() {
        JFileChooser s = new JFileChooser(FileSystemView.getFileSystemView());
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Texto","txt","text");
        s.setFileFilter(filter);
        
        int valS = s.showSaveDialog(null);
        if (valS == JFileChooser.APPROVE_OPTION) {
            try {
                String path= s.getCurrentDirectory() + "\\" + s.getSelectedFile().getName();
                if(!path.contains(".txt")){path = path.concat(".txt");}
                File txt = new File(path);

                if (txt.exists()) {
                    try (FileWriter doc = new FileWriter(path)) {
                        int ele = JOptionPane.showConfirmDialog(null, "Archivo existente, deseas sobreescribirlo?");
                        if (ele == JOptionPane.YES_OPTION) {
                            jTextEditor.write(doc);
                        }
                    }
                }
                if (!txt.exists()) {
                    try (FileWriter doc = new FileWriter(path)) {
                        jTextEditor.write(doc);
                    }
                }

            } catch (HeadlessException | IOException e) {
                JOptionPane.showMessageDialog(null, "Error inesperado: " + e);
            }
        } /*manda un false si el usuario cancela el guardado, ya sea para seguir trabajando
        o por error, el programa no se cierra, espera la confirmacion de nuevo*/ else {
            return false;
        }
        /*si el programa guarda con exito manda true y cierra el programa*/
        return true;
    }

    public void abrir() {
       
        JFileChooser j = new JFileChooser(FileSystemView.getFileSystemView());
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Texto","txt","text");
        j.setFileFilter(filter);
        int valO = j.showOpenDialog(null);
        if (valO == JFileChooser.APPROVE_OPTION) {
            try {
                File arch = new File(j.getCurrentDirectory() + "\\" + j.getSelectedFile().getName());
                
                Scanner read = new Scanner(arch);
                String data = "";
                while (read.hasNextLine()) {
                    data += read.nextLine() + "\n";
                }
                jTextEditor.setText(data);
                this.setTitle("Inter-Es - " + j.getSelectedFile().getName());

                read.close();
            } catch (FileNotFoundException e) {
                JOptionPane.showMessageDialog(null, "Error inesperado: " + e);
            }
        }
        try {

        lh.leng(jTextEditor.getStyledDocument());    
        } catch (Exception e) {
        }
    }

    public void buscarT(String bus) {

        int n1 = 0;
        Pattern pat = Pattern.compile(bus);
        try {
            StyledDocument doc = jTextEditor.getStyledDocument();
            int dim = doc.getLength();
            String txt = doc.getText(0, dim);
            Matcher mat = pat.matcher(txt);
            while (mat.find()) {
                doc.setCharacterAttributes(mat.start(), mat.end() - mat.start(), resaltado, false);
                n1++;
            }
            jTextPane4.setText("Número de coincidencias para " + "\'" + bus + "\'" + " : " + n1);
            
        } catch (BadLocationException e) {
        }
    }

    public void buscarTR(String bus, String remp) {

        int n1 = 0;
        Pattern pat = Pattern.compile("\\b" + bus + "\\b");
        try {
            StyledDocument doc = jTextEditor.getStyledDocument();
            int dim = doc.getLength();
            String txt = doc.getText(0, dim);
            Matcher mat = pat.matcher(txt);
            while (mat.find()) {
                n1++;
            }
            jTextEditor.setText(mat.replaceAll(remp));
            jTextPane5.setText("Número de reemplazos para " + "\'" + bus + "\'" + " : " + n1);
            lh.leng(jTextEditor.getStyledDocument());/*actualizar resaltado*/
        } catch (BadLocationException e) {
        }
    }

    public Grafica() {
        /*setea la fuente y tamaño de los mensajes emergentes*/
        UIManager.put("OptionPane.messageFont", new FontUIResource(new Font("Tahoma", Font.PLAIN, 14)));
        /*setea la fuente del tamaño de los botones de los mensajes emergentes*/
        UIManager.put("OptionPane.buttonFont", new FontUIResource(new Font("Segoe UI Light", Font.PLAIN, 13)));
        /*setea la fuente y tamaño de los textbox en los mensajes emergentes*/
        UIManager.put("TextField.font", new FontUIResource(new Font("Segoe UI", Font.PLAIN, 15)));
        /*tamaño de la fuente del titulo de los formularios o ventanas mas grandes*/
        UIManager.put("Label.font", new FontUIResource(new Font("Segoe UI Light", Font.PLAIN, 14)));
        /*setear el tamaño de las sugerencias de autocompletado*/
        UIManager.put("List.font", new FontUIResource(new Font("Segoe UI", Font.PLAIN, 14)));

        //color de fondo del jframe
        //this.getContentPane().setBackground(new Color(247,247,247));
        /*setea el icono del editor, cambia el logo de java que esta por defecto*/
        URL iconURL = getClass().getResource("I-ES.png");
        ImageIcon icon = new ImageIcon(iconURL);
        this.setIconImage(icon.getImage());

        initComponents();
        /*setear icono, logo del programa de las demas ventanas emergentes*/
        jvVariables.setIconImage(icon.getImage());
        jvBuscar.setIconImage(icon.getImage());
        jvBusRem.setIconImage(icon.getImage());
        jvAyuda.setIconImage(icon.getImage());
        jvResNot.setIconImage(icon.getImage());
        jvColor.setIconImage(icon.getImage());
        jvFuente.setIconImage(icon.getImage());
        //cambia la operacion del jframe antes de cerrar, no hace nada para que el usuario elija que hacer
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        //numeros en el editor o jframe
        setNumbers();
        jTextEditor.requestFocus();
        //listener destinado a las operaciones de hacer y deshacer
    
        jTextEditor.getDocument().addUndoableEditListener(um);
        // estilo resaltado de busqueda
        StyleConstants.setBackground(resaltado, new Color(144, 202, 249));
        StyleConstants.setBackground(normal, Color.BLUE);
        /*burbujitas flotantes con pistas para el usuario en los botones*/
        jbEjecuta.setToolTipText("Ejecutar Programa (F5)");
        jbLimpiar.setToolTipText("Presiona este botón para borrar las notificaciones");
        jbAbrirb.setToolTipText("Abrir (Ctrl+O)");
        jbGuardarb.setToolTipText("Guardar como... (Ctrl+S)");
        jbDeshacer.setToolTipText("Deshacer (Ctrl+Z)");
        jbRehacer.setToolTipText("Rehacer (Ctrl+Y)");
        jbSelect.setToolTipText("Seleccionar todo (Ctrl+E)");
        jbCopiar.setToolTipText("Copiar (Ctrl+C)");
        jbPegar.setToolTipText("Pegar (Ctrl+V)");
        jbCalc.setToolTipText("Abrir calculadora del sistema");
        jbTxt.setToolTipText("Abrir bloc de notas del sistema");
        jbAnaliza.setToolTipText("Verificar Sintaxis del programa");
        jTextLVar.setToolTipText("Variables usadas en tu programa");
        jLPlantilla.setToolTipText("Selecciona e inserta un componente a tu programa");
        jTree1.setToolTipText("Doble click para insertar al editor de código");
        //centrar en dispositivo las ventanas emergentes
        this.setLocationRelativeTo(null);
        jvVariables.setLocationRelativeTo(null);
        jvBuscar.setLocationRelativeTo(null);
        jvBusRem.setLocationRelativeTo(null);
        jvAyuda.setLocationRelativeTo(null);
        jvResNot.setLocationRelativeTo(null);
        jvColor.setLocationRelativeTo(null);
        jvFuente.setLocationRelativeTo(null);
        /*se inicia el panel de autocompletado*/
        autoCompletado aa = new autoCompletado();
        CompletionProvider provider = aa.montarAutocompletado();
        AutoCompletion ac = new AutoCompletion(provider);
        ac.install(jTextEditor);
        //declarar donde sale el resaltador de linea
        lp = new LinePainter(jTextEditor);
        jTextEditor.setSelectionColor(new Color(56,173,177));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jvVariables = new javax.swing.JDialog();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextvLVar = new javax.swing.JTextPane();
        jLabel2 = new javax.swing.JLabel();
        jvBuscar = new javax.swing.JDialog();
        jLabel3 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jButton13 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextPane4 = new javax.swing.JTextPane();
        jvBusRem = new javax.swing.JDialog();
        jLabel4 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jButton15 = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextPane5 = new javax.swing.JTextPane();
        jLabel5 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jvAyuda = new javax.swing.JDialog();
        jScrollPane8 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jTextPane7 = new javax.swing.JTextPane();
        jvResNot = new javax.swing.JDialog();
        jScrollPane10 = new javax.swing.JScrollPane();
        jTextvResNot = new javax.swing.JTextPane();
        jvColor = new javax.swing.JDialog();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jButton18 = new javax.swing.JButton();
        jColorChooser1 = new javax.swing.JColorChooser();
        jButton22 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jScrollPane15 = new javax.swing.JScrollPane();
        jTextColors = new javax.swing.JTextPane();
        jButton2 = new javax.swing.JButton();
        jvFuente = new javax.swing.JDialog();
        jScrollPane11 = new javax.swing.JScrollPane();
        jLFuente = new javax.swing.JList<>();
        jScrollPane12 = new javax.swing.JScrollPane();
        jLEstilo = new javax.swing.JList<>();
        jScrollPane13 = new javax.swing.JScrollPane();
        jLTamanio = new javax.swing.JList<>();
        jScrollPane14 = new javax.swing.JScrollPane();
        jTextPane9 = new javax.swing.JTextPane();
        jButton19 = new javax.swing.JButton();
        jButton20 = new javax.swing.JButton();
        jComboBox2 = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jButton21 = new javax.swing.JButton();
        jMenuC1 = new javax.swing.JPopupMenu();
        jMenuItem15 = new javax.swing.JMenuItem();
        jMenuItem16 = new javax.swing.JMenuItem();
        jMenuItem17 = new javax.swing.JMenuItem();
        jMenuItem18 = new javax.swing.JMenuItem();
        jMenuItem30 = new javax.swing.JMenuItem();
        jMenuItem20 = new javax.swing.JMenuItem();
        jMenuItem27 = new javax.swing.JMenuItem();
        jMenuC2 = new javax.swing.JPopupMenu();
        jMenuItem24 = new javax.swing.JMenuItem();
        jMenuItem26 = new javax.swing.JMenuItem();
        jMenuLP = new javax.swing.JPopupMenu();
        jMenuItem23 = new javax.swing.JMenuItem();
        jToolBar2 = new javax.swing.JToolBar();
        jbDeshacer = new javax.swing.JButton();
        jbRehacer = new javax.swing.JButton();
        jbSelect = new javax.swing.JButton();
        jbCopiar = new javax.swing.JButton();
        jbPegar = new javax.swing.JButton();
        jbCortar = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        jbLimpiar = new javax.swing.JButton();
        jbAbrirb = new javax.swing.JButton();
        jbGuardarb = new javax.swing.JButton();
        jbEjecuta = new javax.swing.JButton();
        jbAnaliza = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JToolBar.Separator();
        jbCalc = new javax.swing.JButton();
        jbTxt = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jSplitPane4 = new javax.swing.JSplitPane();
        jSplitPane3 = new javax.swing.JSplitPane();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTextLVar = new javax.swing.JTextPane();
        jScrollPane7 = new javax.swing.JScrollPane();
        jLPlantilla = new javax.swing.JList<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextEditor = new javax.swing.JTextPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTextNotRes = new javax.swing.JTextPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        jmArchivo = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jmEdicion = new javax.swing.JMenu();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenuItem19 = new javax.swing.JMenuItem();
        jMenuItem21 = new javax.swing.JMenuItem();
        jMenuItem22 = new javax.swing.JMenuItem();
        jmConfig = new javax.swing.JMenu();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem28 = new javax.swing.JMenuItem();
        jMenuItem33 = new javax.swing.JMenuItem();
        jMenuItem29 = new javax.swing.JMenuItem();
        jMenuItem34 = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        jMenuItem32 = new javax.swing.JMenuItem();
        jMenuItem31 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        jMenuItem9 = new javax.swing.JMenuItem();
        jmAyuda = new javax.swing.JMenu();
        jMenuItem25 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();

        jvVariables.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        jvVariables.setTitle("Variables");
        jvVariables.setAlwaysOnTop(true);
        jvVariables.setName("Resultados"); // NOI18N
        jvVariables.setResizable(false);
        jvVariables.setSize(new java.awt.Dimension(450, 366));

        jTextvLVar.setEditable(false);
        jTextvLVar.setFont(new java.awt.Font("Segoe UI Light", 0, 16)); // NOI18N
        jScrollPane1.setViewportView(jTextvLVar);

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        jLabel2.setText("Variables de acuerdo a tipo, identificador y valor asignado");

        javax.swing.GroupLayout jvVariablesLayout = new javax.swing.GroupLayout(jvVariables.getContentPane());
        jvVariables.getContentPane().setLayout(jvVariablesLayout);
        jvVariablesLayout.setHorizontalGroup(
            jvVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jvVariablesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jvVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 424, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addContainerGap(15, Short.MAX_VALUE))
        );
        jvVariablesLayout.setVerticalGroup(
            jvVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jvVariablesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(229, 229, 229))
        );

        jvBuscar.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        jvBuscar.setTitle("Buscar");
        jvBuscar.setAlwaysOnTop(true);
        jvBuscar.setResizable(false);
        jvBuscar.setSize(new java.awt.Dimension(551, 158));

        jLabel3.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jLabel3.setText("Buscar");

        jTextField1.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N

        jButton13.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jButton13.setText("Busqueda");
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });

        jTextPane4.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jScrollPane2.setViewportView(jTextPane4);

        javax.swing.GroupLayout jvBuscarLayout = new javax.swing.GroupLayout(jvBuscar.getContentPane());
        jvBuscar.getContentPane().setLayout(jvBuscarLayout);
        jvBuscarLayout.setHorizontalGroup(
            jvBuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jvBuscarLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jvBuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2)
                    .addGroup(jvBuscarLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 339, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        jvBuscarLayout.setVerticalGroup(
            jvBuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jvBuscarLayout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jvBuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(35, Short.MAX_VALUE))
        );

        jvBusRem.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        jvBusRem.setTitle("Buscar y reemplazar");
        jvBusRem.setAlwaysOnTop(true);
        jvBusRem.setResizable(false);
        jvBusRem.setSize(new java.awt.Dimension(559, 185));

        jLabel4.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jLabel4.setText("Buscar");

        jTextField2.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N

        jButton15.setFont(new java.awt.Font("Segoe UI Light", 0, 13)); // NOI18N
        jButton15.setText("Reemplazar");
        jButton15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton15ActionPerformed(evt);
            }
        });

        jTextPane5.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jScrollPane5.setViewportView(jTextPane5);

        jLabel5.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jLabel5.setText("Reemplazar con:");

        jTextField3.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N

        javax.swing.GroupLayout jvBusRemLayout = new javax.swing.GroupLayout(jvBusRem.getContentPane());
        jvBusRem.getContentPane().setLayout(jvBusRemLayout);
        jvBusRemLayout.setHorizontalGroup(
            jvBusRemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jvBusRemLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jvBusRemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 503, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jvBusRemLayout.createSequentialGroup()
                        .addGroup(jvBusRemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jvBusRemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField2, javax.swing.GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE)
                            .addComponent(jTextField3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton15, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        jvBusRemLayout.setVerticalGroup(
            jvBusRemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jvBusRemLayout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jvBusRemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton15, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jvBusRemLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        jvAyuda.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        jvAyuda.setTitle("Ayuda con ejemplos Inter-Es");
        jvAyuda.setAlwaysOnTop(true);
        jvAyuda.setResizable(false);
        jvAyuda.setSize(new java.awt.Dimension(850, 540));

        jTree1.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("Acerca de...");
        javax.swing.tree.DefaultMutableTreeNode treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("¿Para qué sirve este apartado?");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Introducción");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Motivación");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Comentarios de código");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Tipos de datos admitidos");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Sentencia 'Muestra'");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Sentencia 'Ingresa'");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Operaciones básicas aritméticas");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Operaciones con cadenas");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Variables/Asignacion");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Funciones aritméticas");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Estructuras de control (Si, Sino)");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Estructuras de repetición (Mientras, Repite)");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Estructuras control y repetición (Si, Mientras)");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Depuración (Apartado Variables)");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Conversion de tipos (Numerico, Cadena, Entero)");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Número par o impar (Operador residual 'd% ')");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Tabla de multiplicar (Conversión combinada)");
        treeNode1.add(treeNode2);
        treeNode2 = new javax.swing.tree.DefaultMutableTreeNode("Condición binaria (AND-OR)");
        treeNode1.add(treeNode2);
        jTree1.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        jTree1.setVisibleRowCount(30);
        jTree1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTree1MouseClicked(evt);
            }
        });
        jScrollPane8.setViewportView(jTree1);

        jLabel8.setFont(new java.awt.Font("Segoe UI Light", 2, 14)); // NOI18N
        jLabel8.setText("Para más detalles consultar el manual de uso");

        jTextPane7.setEditable(false);
        jTextPane7.setFont(new java.awt.Font("Consolas", 0, 15)); // NOI18N
        jTextPane7.setAutoscrolls(false);
        jScrollPane9.setViewportView(jTextPane7);

        javax.swing.GroupLayout jvAyudaLayout = new javax.swing.GroupLayout(jvAyuda.getContentPane());
        jvAyuda.getContentPane().setLayout(jvAyudaLayout);
        jvAyudaLayout.setHorizontalGroup(
            jvAyudaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jvAyudaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jvAyudaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jvAyudaLayout.createSequentialGroup()
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 337, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 487, Short.MAX_VALUE))
                    .addGroup(jvAyudaLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jvAyudaLayout.setVerticalGroup(
            jvAyudaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jvAyudaLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jvAyudaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane9)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel8)
                .addGap(32, 32, 32))
        );

        jvResNot.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        jvResNot.setTitle("Resultados / Notificaciones");
        jvResNot.setAlwaysOnTop(true);
        jvResNot.setSize(new java.awt.Dimension(795, 540));

        jTextvResNot.setEditable(false);
        jTextvResNot.setContentType("text/html"); // NOI18N
        jTextvResNot.setFont(new java.awt.Font("Segoe UI Light", 0, 20)); // NOI18N
        jScrollPane10.setViewportView(jTextvResNot);

        javax.swing.GroupLayout jvResNotLayout = new javax.swing.GroupLayout(jvResNot.getContentPane());
        jvResNot.getContentPane().setLayout(jvResNotLayout);
        jvResNotLayout.setHorizontalGroup(
            jvResNotLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jvResNotLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 745, Short.MAX_VALUE)
                .addGap(23, 23, 23))
        );
        jvResNotLayout.setVerticalGroup(
            jvResNotLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jvResNotLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 443, Short.MAX_VALUE)
                .addGap(59, 59, 59))
        );

        jvColor.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        jvColor.setTitle("Personalizar colores");
        jvColor.setAlwaysOnTop(true);
        jvColor.setResizable(false);
        jvColor.setSize(new java.awt.Dimension(950, 550));
        jvColor.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                jvColorWindowOpened(evt);
            }
        });

        jComboBox1.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Color del resaltador de texto", "Color de fondo del editor", "Color de las palabras reservadas", "Color de las cadenas de texto", "Color de fuente del editor ", "Color de los comentarios" }));

        jLabel1.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jLabel1.setText("Componente a editar:");

        jButton18.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jButton18.setText("Aplicar");
        jButton18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton18ActionPerformed(evt);
            }
        });

        jButton22.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jButton22.setText("Cancelar");
        jButton22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton22ActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jButton1.setText("Aceptar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTextColors.setEditable(false);
        jTextColors.setFont(new java.awt.Font("Consolas", 0, 15)); // NOI18N
        jTextColors.setText("/*comentario de ejemplo*/\n\n//otro comentario unilinea\n\nnumerico n1 = 20\nnumerico n2 = 22\n\nsi n1 > n2 entonces\nmuestra \"n1 es mayor\"\nsino\nmuestra \"n2 es mayor\"\nfinsi\n\nmuestra (cadena: n1) & \" n1\" .");
        jScrollPane15.setViewportView(jTextColors);

        jButton2.setFont(new java.awt.Font("Segoe UI Light", 1, 14)); // NOI18N
        jButton2.setText("?");
        jButton2.setToolTipText("Define a qué componente o parte del programa se aplicará \nla configuración de color definida.");

        javax.swing.GroupLayout jvColorLayout = new javax.swing.GroupLayout(jvColor.getContentPane());
        jvColor.getContentPane().setLayout(jvColorLayout);
        jvColorLayout.setHorizontalGroup(
            jvColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jvColorLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jvColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(jvColorLayout.createSequentialGroup()
                        .addGroup(jvColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jvColorLayout.createSequentialGroup()
                                .addComponent(jButton18, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton22, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jColorChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 640, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jvColorLayout.createSequentialGroup()
                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 322, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton2)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(138, Short.MAX_VALUE))
        );
        jvColorLayout.setVerticalGroup(
            jvColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jvColorLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jLabel1)
                .addGap(12, 12, 12)
                .addGroup(jvColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jvColorLayout.createSequentialGroup()
                        .addGroup(jvColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                            .addComponent(jButton2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jColorChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jvColorLayout.createSequentialGroup()
                        .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jvColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton18)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton22))
                .addGap(321, 321, 321))
        );

        jvFuente.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        jvFuente.setTitle("Personalizar Fuente");
        jvFuente.setAlwaysOnTop(true);
        jvFuente.setResizable(false);
        jvFuente.setSize(new java.awt.Dimension(599, 560));
        jvFuente.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                jvFuenteWindowOpened(evt);
            }
        });

        jLFuente.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jLFuente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLFuenteMouseClicked(evt);
            }
        });
        jScrollPane11.setViewportView(jLFuente);

        jLEstilo.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jLEstilo.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Normal", "Negrita", "Cursiva", " " };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jLEstilo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLEstiloMouseClicked(evt);
            }
        });
        jScrollPane12.setViewportView(jLEstilo);

        jLTamanio.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jLTamanio.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "10", "12", "14", "16", "18", "20", "22", "24", "26", "28", "30", "36" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jLTamanio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLTamanioMouseClicked(evt);
            }
        });
        jScrollPane13.setViewportView(jLTamanio);

        jTextPane9.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        jTextPane9.setText("Este es un texto de ejemplo: 123456!@$");
        jScrollPane14.setViewportView(jTextPane9);

        jButton19.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jButton19.setText("Aceptar");
        jButton19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton19ActionPerformed(evt);
            }
        });

        jButton20.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jButton20.setText("Cancelar");
        jButton20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton20ActionPerformed(evt);
            }
        });

        jComboBox2.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Editor de código", "Resultados/Notificaciones", "Ventana de Resultados/Notificaciones", "Panel de variables" }));

        jLabel7.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jLabel7.setText("Fuente");

        jLabel9.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jLabel9.setText("Estilo");

        jLabel10.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jLabel10.setText("Tamaño");

        jLabel11.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jLabel11.setText("Componente a editar");

        jLabel12.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jLabel12.setText("Vista previa");

        jButton21.setFont(new java.awt.Font("Segoe UI Light", 1, 14)); // NOI18N
        jButton21.setText("?");
        jButton21.setToolTipText("Define a qué componente o parte del programa se aplicará \nla configuración de fuente definida.");

        javax.swing.GroupLayout jvFuenteLayout = new javax.swing.GroupLayout(jvFuente.getContentPane());
        jvFuente.getContentPane().setLayout(jvFuenteLayout);
        jvFuenteLayout.setHorizontalGroup(
            jvFuenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jvFuenteLayout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addGroup(jvFuenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addGroup(jvFuenteLayout.createSequentialGroup()
                        .addGroup(jvFuenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane14)
                            .addGroup(jvFuenteLayout.createSequentialGroup()
                                .addComponent(jButton19, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton20, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jComboBox2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jvFuenteLayout.createSequentialGroup()
                                .addGroup(jvFuenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel7))
                                .addGap(31, 31, 31)
                                .addGroup(jvFuenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel9))
                                .addGap(29, 29, 29)
                                .addGroup(jvFuenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel10)
                                    .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton21, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel11))
                .addContainerGap(26, Short.MAX_VALUE))
        );
        jvFuenteLayout.setVerticalGroup(
            jvFuenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jvFuenteLayout.createSequentialGroup()
                .addContainerGap(37, Short.MAX_VALUE)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jvFuenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jvFuenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jvFuenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jvFuenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jScrollPane13, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel12)
                .addGap(3, 3, 3)
                .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jvFuenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton19)
                    .addComponent(jButton20))
                .addGap(25, 25, 25))
        );

        jMenuC1.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N

        jMenuItem15.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
        jMenuItem15.setText("Seleccionar todo");
        jMenuItem15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem15ActionPerformed(evt);
            }
        });
        jMenuC1.add(jMenuItem15);

        jMenuItem16.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
        jMenuItem16.setText("Copiar");
        jMenuItem16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem16ActionPerformed(evt);
            }
        });
        jMenuC1.add(jMenuItem16);

        jMenuItem17.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
        jMenuItem17.setText("Pegar");
        jMenuItem17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem17ActionPerformed(evt);
            }
        });
        jMenuC1.add(jMenuItem17);

        jMenuItem18.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
        jMenuItem18.setText("Cortar");
        jMenuItem18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem18ActionPerformed(evt);
            }
        });
        jMenuC1.add(jMenuItem18);

        jMenuItem30.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jMenuItem30.setText("Ejecutar programa");
        jMenuItem30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem30ActionPerformed(evt);
            }
        });
        jMenuC1.add(jMenuItem30);

        jMenuItem20.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
        jMenuItem20.setText("Buscar...");
        jMenuItem20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem20ActionPerformed(evt);
            }
        });
        jMenuC1.add(jMenuItem20);

        jMenuItem27.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
        jMenuItem27.setText("Autocompletar...");
        jMenuItem27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem27ActionPerformed(evt);
            }
        });
        jMenuC1.add(jMenuItem27);

        jMenuItem24.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
        jMenuItem24.setText("Limpiar");
        jMenuItem24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem24ActionPerformed(evt);
            }
        });
        jMenuC2.add(jMenuItem24);

        jMenuItem26.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
        jMenuItem26.setText("Mostrar en ventana grande");
        jMenuItem26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem26ActionPerformed(evt);
            }
        });
        jMenuC2.add(jMenuItem26);

        jMenuItem23.setFont(new java.awt.Font("Segoe UI", 0, 13)); // NOI18N
        jMenuItem23.setText("Añadir al programa");
        jMenuItem23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem23ActionPerformed(evt);
            }
        });
        jMenuLP.add(jMenuItem23);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Inter-Es");
        setBackground(new java.awt.Color(204, 204, 204));
        setLocation(new java.awt.Point(400, 15));
        setPreferredSize(new java.awt.Dimension(970, 730));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jToolBar2.setRollover(true);

        jbDeshacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/undo.png"))); // NOI18N
        jbDeshacer.setFocusable(false);
        jbDeshacer.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbDeshacer.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbDeshacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbDeshacerActionPerformed(evt);
            }
        });
        jToolBar2.add(jbDeshacer);

        jbRehacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/redo.png"))); // NOI18N
        jbRehacer.setFocusable(false);
        jbRehacer.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbRehacer.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbRehacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbRehacerActionPerformed(evt);
            }
        });
        jToolBar2.add(jbRehacer);

        jbSelect.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/select-all.png"))); // NOI18N
        jbSelect.setFocusable(false);
        jbSelect.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbSelect.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbSelectActionPerformed(evt);
            }
        });
        jToolBar2.add(jbSelect);

        jbCopiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/copy.png"))); // NOI18N
        jbCopiar.setFocusable(false);
        jbCopiar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbCopiar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbCopiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbCopiarActionPerformed(evt);
            }
        });
        jToolBar2.add(jbCopiar);

        jbPegar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/paste.png"))); // NOI18N
        jbPegar.setFocusable(false);
        jbPegar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbPegar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbPegar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbPegarActionPerformed(evt);
            }
        });
        jToolBar2.add(jbPegar);

        jbCortar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/cut.png"))); // NOI18N
        jbCortar.setFocusable(false);
        jbCortar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbCortar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbCortar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbCortarActionPerformed(evt);
            }
        });
        jToolBar2.add(jbCortar);
        jToolBar2.add(jSeparator2);

        jbLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/clear.png"))); // NOI18N
        jbLimpiar.setAlignmentX(6.0F);
        jbLimpiar.setFocusable(false);
        jbLimpiar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbLimpiar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbLimpiarActionPerformed(evt);
            }
        });
        jToolBar2.add(jbLimpiar);

        jbAbrirb.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/carpeta.png"))); // NOI18N
        jbAbrirb.setFocusable(false);
        jbAbrirb.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbAbrirb.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbAbrirb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbAbrirbActionPerformed(evt);
            }
        });
        jToolBar2.add(jbAbrirb);

        jbGuardarb.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/disquete.png"))); // NOI18N
        jbGuardarb.setFocusable(false);
        jbGuardarb.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbGuardarb.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbGuardarb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbGuardarbActionPerformed(evt);
            }
        });
        jToolBar2.add(jbGuardarb);

        jbEjecuta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/play_1.png"))); // NOI18N
        jbEjecuta.setFocusable(false);
        jbEjecuta.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbEjecuta.setIconTextGap(2);
        jbEjecuta.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbEjecuta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbEjecutaActionPerformed(evt);
            }
        });
        jToolBar2.add(jbEjecuta);

        jbAnaliza.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/syntaxc.png"))); // NOI18N
        jbAnaliza.setFocusable(false);
        jbAnaliza.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbAnaliza.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbAnaliza.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbAnalizaActionPerformed(evt);
            }
        });
        jToolBar2.add(jbAnaliza);
        jToolBar2.add(jSeparator3);

        jbCalc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/calculator.png"))); // NOI18N
        jbCalc.setFocusable(false);
        jbCalc.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbCalc.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbCalc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbCalcActionPerformed(evt);
            }
        });
        jToolBar2.add(jbCalc);

        jbTxt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/notepad.png"))); // NOI18N
        jbTxt.setFocusable(false);
        jbTxt.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbTxt.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbTxtActionPerformed(evt);
            }
        });
        jToolBar2.add(jbTxt);

        jLabel6.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jLabel6.setText("Linea/Caracter:");

        jSplitPane4.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jSplitPane3.setPreferredSize(new java.awt.Dimension(211, 421));

        jTabbedPane2.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N

        jTextLVar.setEditable(false);
        jTextLVar.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jTextLVar.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jScrollPane6.setViewportView(jTextLVar);

        jTabbedPane2.addTab("Variables", jScrollPane6);

        jLPlantilla.setFont(new java.awt.Font("Segoe UI Light", 1, 14)); // NOI18N
        jLPlantilla.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Si", "Si/Sino", "Si (Binario) AND", "Si (Binario) OR", "Mientras", "Repite", "Repite (Con Condición)", "Raíz2 (Raiz Cuadrada)", "Raíz3 (Raiz Cúbica)", "Azar (Número aleatorio)", "Pi", "Muestra", "Ingresa  (Con Mensaje)", "Ingresa (Sin Mensaje)", "Convertir a Cadena", "Convertir a Numérico", "Entero (Redondeo)", "> (Mayor que)", "< (Menor que)", ">= (Mayor o igual que)", "<= (Menor o igual que)", "!= (Diferente de)", "d% (Residual)", "& (Concatenar)", "&& (AND lógico)", "|| (OR lógico)", "/ (División)", "+ (Suma)", "- (Resta)", "* (Multiplicación)", "== (Igual Que, números o cadenas)" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jLPlantilla.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jLPlantilla.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLPlantillaMousePressed(evt);
            }
        });
        jScrollPane7.setViewportView(jLPlantilla);

        jTabbedPane2.addTab("Plantillas", jScrollPane7);

        jSplitPane3.setLeftComponent(jTabbedPane2);

        jScrollPane3.setFont(new java.awt.Font("Segoe UI Light", 0, 13)); // NOI18N

        jTextEditor.setFont(new java.awt.Font("Segoe UI Light", 0, 12)); // NOI18N
        jTextEditor.setDragEnabled(true);
        jTextEditor.setMinimumSize(new java.awt.Dimension(3, 15));
        jTextEditor.setPreferredSize(new java.awt.Dimension(3, 15));
        jTextEditor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextEditorMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTextEditorMousePressed(evt);
            }
        });
        jTextEditor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextEditorKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTextEditorKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextEditorKeyTyped(evt);
            }
        });
        jScrollPane3.setViewportView(jTextEditor);

        jSplitPane3.setRightComponent(jScrollPane3);

        jSplitPane4.setLeftComponent(jSplitPane3);

        jScrollPane4.setFont(new java.awt.Font("Segoe UI Light", 0, 13)); // NOI18N

        jTextNotRes.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Notificaciones / Resultados:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI Light", 1, 15), new java.awt.Color(255, 102, 0))); // NOI18N
        jTextNotRes.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        jTextNotRes.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jTextNotRes.setFocusable(false);
        jTextNotRes.setMinimumSize(new java.awt.Dimension(3, 15));
        jTextNotRes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTextNotResMousePressed(evt);
            }
        });
        jScrollPane4.setViewportView(jTextNotRes);

        jSplitPane4.setBottomComponent(jScrollPane4);

        jmArchivo.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jmArchivo.setLabel("Archivo");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem1.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/carpeta.png"))); // NOI18N
        jMenuItem1.setText("Abrir programa");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jmArchivo.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem2.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/disquete.png"))); // NOI18N
        jMenuItem2.setText("Guardar como...");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jmArchivo.add(jMenuItem2);

        jMenuBar1.add(jmArchivo);

        jmEdicion.setText("Edición");
        jmEdicion.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N

        jMenuItem13.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem13.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/select-all.png"))); // NOI18N
        jMenuItem13.setText("Seleccionar todo");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        jmEdicion.add(jMenuItem13);

        jMenuItem10.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem10.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/undo.png"))); // NOI18N
        jMenuItem10.setText("Deshacer");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jmEdicion.add(jMenuItem10);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Y, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem3.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/redo.png"))); // NOI18N
        jMenuItem3.setText("Rehacer");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jmEdicion.add(jMenuItem3);

        jMenuItem11.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem11.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/copy.png"))); // NOI18N
        jMenuItem11.setText("Copiar");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jmEdicion.add(jMenuItem11);

        jMenuItem12.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem12.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/paste.png"))); // NOI18N
        jMenuItem12.setText("Pegar");
        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem12ActionPerformed(evt);
            }
        });
        jmEdicion.add(jMenuItem12);

        jMenuItem14.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem14.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/cut.png"))); // NOI18N
        jMenuItem14.setText("Cortar");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        jmEdicion.add(jMenuItem14);

        jMenuItem19.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem19.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/magnifying-glass_1.png"))); // NOI18N
        jMenuItem19.setText("Buscar...");
        jMenuItem19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem19ActionPerformed(evt);
            }
        });
        jmEdicion.add(jMenuItem19);

        jMenuItem21.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.SHIFT_DOWN_MASK | java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem21.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/switch.png"))); // NOI18N
        jMenuItem21.setText("Buscar y reemplazar");
        jMenuItem21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem21ActionPerformed(evt);
            }
        });
        jmEdicion.add(jMenuItem21);

        jMenuItem22.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem22.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/font-size.png"))); // NOI18N
        jMenuItem22.setText("Pasar de mayúsculas a minúsculas");
        jMenuItem22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem22ActionPerformed(evt);
            }
        });
        jmEdicion.add(jMenuItem22);

        jMenuBar1.add(jmEdicion);

        jmConfig.setText("Configuración");
        jmConfig.setActionCommand("");
        jmConfig.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/color.png"))); // NOI18N
        jMenu5.setText("Apariencia");
        jMenu5.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem4.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem4.setText("Tema Blanco");
        jMenuItem4.setToolTipText("");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem4);

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F2, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem5.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem5.setText("Tema Gris");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem5);

        jMenuItem28.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem28.setText("Tema Gris Oscuro");
        jMenuItem28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem28ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem28);

        jMenuItem33.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem33.setText("Tema Azul");
        jMenuItem33.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem33ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem33);

        jMenuItem29.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem29.setText("Tema Solarizado");
        jMenuItem29.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem29ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem29);

        jMenuItem34.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem34.setText("Tema Alto Contraste");
        jMenuItem34.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem34ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem34);

        jSeparator4.setForeground(new java.awt.Color(153, 153, 153));
        jMenu5.add(jSeparator4);

        jMenuItem32.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/colourf.png"))); // NOI18N
        jMenuItem32.setText("Personalizar Colores del Entorno");
        jMenuItem32.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem32ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem32);

        jMenuItem31.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem31.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/fontt.png"))); // NOI18N
        jMenuItem31.setText("Personalizar Fuente del Entorno");
        jMenuItem31.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem31ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem31);

        jmConfig.add(jMenu5);

        jMenuItem7.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_W, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem7.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/plus.png"))); // NOI18N
        jMenuItem7.setText("Acercar");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jmConfig.add(jMenuItem7);

        jMenuItem8.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem8.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/minus.png"))); // NOI18N
        jMenuItem8.setText("Alejar");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jmConfig.add(jMenuItem8);

        jSeparator5.setForeground(new java.awt.Color(153, 153, 153));
        jmConfig.add(jSeparator5);

        jMenuItem9.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItem9.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/code-error.png"))); // NOI18N
        jMenuItem9.setText("Variables");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jmConfig.add(jMenuItem9);

        jMenuBar1.add(jmConfig);

        jmAyuda.setText("Ayuda");
        jmAyuda.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jmAyuda.addMenuListener(new javax.swing.event.MenuListener() {
            public void menuCanceled(javax.swing.event.MenuEvent evt) {
            }
            public void menuDeselected(javax.swing.event.MenuEvent evt) {
            }
            public void menuSelected(javax.swing.event.MenuEvent evt) {
                jmAyudaMenuSelected(evt);
            }
        });

        jMenuItem25.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem25.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/formula.png"))); // NOI18N
        jMenuItem25.setText("Ayuda con ejemplos de Inter-Es");
        jMenuItem25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem25ActionPerformed(evt);
            }
        });
        jmAyuda.add(jMenuItem25);

        jMenuItem6.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F12, 0));
        jMenuItem6.setFont(new java.awt.Font("Segoe UI Light", 0, 14)); // NOI18N
        jMenuItem6.setText("Acerca de...");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jmAyuda.add(jMenuItem6);

        jMenuBar1.add(jmAyuda);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jToolBar2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jSplitPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 958, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(jSplitPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 644, Short.MAX_VALUE)
                .addContainerGap())
        );

        getAccessibleContext().setAccessibleName("Editor");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbEjecutaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbEjecutaActionPerformed
        // TODO add your handling code here:
        //ejecutar programa
        ejecutarPr();
    }//GEN-LAST:event_jbEjecutaActionPerformed

    private void jmAyudaMenuSelected(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_jmAyudaMenuSelected
        // TODO add your handling code here:

    }//GEN-LAST:event_jmAyudaMenuSelected

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        abrir();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        guardar();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jbLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbLimpiarActionPerformed
        // TODO add your handling code here:
        jTextNotRes.setText("");
        stringrs = "";
    }//GEN-LAST:event_jbLimpiarActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        // TODO add your handling code here:
        //fondo claro
        selectTema("Blanco");
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // TODO add your handling code here:
        //fondo gris
        selectTema("Gris");
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:

        if (!jTextEditor.getText().equals("")) {
            int res = JOptionPane.showConfirmDialog(null, "¿Deseas guardar los cambios en tu programa?", "Inter-Es ", JOptionPane.YES_NO_CANCEL_OPTION);
            switch (res) {
                case 0:
                    if (guardar() == true) {
                        evt.getWindow().dispose();
                        System.exit(0);
                    }
                    break;
                case 1:
                    evt.getWindow().dispose();
                    System.exit(0);
                    break;

            }
        } else {
            evt.getWindow().dispose();
            System.exit(0);
        }

    }//GEN-LAST:event_formWindowClosing

    private void jbAbrirbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbAbrirbActionPerformed
        // TODO add your handling code here:
        abrir();
    }//GEN-LAST:event_jbAbrirbActionPerformed

    private void jbGuardarbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbGuardarbActionPerformed
        // TODO add your handling code here:
        guardar();
    }//GEN-LAST:event_jbGuardarbActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        // TODO add your handling code here:
        this.tLetra += 1;   
        jTextEditor.setFont(jTextEditor.getFont().deriveFont(Float.parseFloat(String.valueOf(tLetra))));
        
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        // TODO add your handling code here:
        this.tLetra -= 1;
        jTextEditor.setFont(jTextEditor.getFont().deriveFont(Float.parseFloat(String.valueOf(tLetra))));

    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        // TODO add your handling code here:
        JOptionPane.showMessageDialog(null, "Diseñado y desarrollado para CMB."
                + "\nS.T 2022");
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        // TODO add your handling code here:
        //muestra ventana de variables
        jvVariables.show();
    }//GEN-LAST:event_jMenuItem9ActionPerformed
    public void undoableEditHappened(UndoableEditEvent e) {
        um.addEdit(e.getEdit());
    }
    private void jbDeshacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbDeshacerActionPerformed
        // TODO add your handling code here:
        //deshacer
        
        try {
            
               um.undo();    
        } catch (CannotUndoException e) {
        }

    }//GEN-LAST:event_jbDeshacerActionPerformed

    private void jbRehacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbRehacerActionPerformed
        // TODO add your handling code here:
        //rehacer
        try {
            um.redo();
        } catch (CannotRedoException e) {
        }
    }//GEN-LAST:event_jbRehacerActionPerformed

    private void jTextEditorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextEditorKeyPressed
        // TODO add your handling code here:
        //combinacion de teclas de atajo para editar codigo 
        
        if (evt.getKeyCode() == KeyEvent.VK_F5) {
            ejecutarPr();
        }

    }//GEN-LAST:event_jTextEditorKeyPressed

    private void jbSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbSelectActionPerformed
        // TODO add your handling code here:
        /*boton para seleccionar godo*/
        jTextEditor.selectAll();
    }//GEN-LAST:event_jbSelectActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        // TODO add your handling code here:
        try {
            um.undo();
        } catch (CannotUndoException e) {
        }
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        // TODO add your handling code here:
        try {
            um.redo();
        } catch (CannotUndoException e) {
        }
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jbCopiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbCopiarActionPerformed
        // TODO add your handling code here:
        //copiar
        jTextEditor.copy();
    }//GEN-LAST:event_jbCopiarActionPerformed

    private void jbPegarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbPegarActionPerformed
        // TODO add your handling code here:
        //pegar
        try {
            jTextEditor.paste();
            lh.leng(jTextEditor.getStyledDocument());
        } catch (Exception e) {
        }

    }//GEN-LAST:event_jbPegarActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        // TODO add your handling code here:
        jTextEditor.copy();
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jMenuItem12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem12ActionPerformed
        // TODO add your handling code here:
        //pegar
        try {
            jTextEditor.paste();
            lh.leng(jTextEditor.getStyledDocument());
        } catch (Exception e) {
        }
    }//GEN-LAST:event_jMenuItem12ActionPerformed

    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        // TODO add your handling code here:
        jTextEditor.selectAll(); //seleccionar todo
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed
        // TODO add your handling code here:
        //cortar
        jTextEditor.cut();
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    private void jbCortarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbCortarActionPerformed
        // TODO add your handling code here:
        //cortar
        jTextEditor.cut();
    }//GEN-LAST:event_jbCortarActionPerformed

    private void jTextEditorMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextEditorMousePressed
        // TODO add your handling code here:
        if (SwingUtilities.isRightMouseButton(evt)) {
            jMenuC1.show(jTextEditor, evt.getX(), evt.getY());

        }
    }//GEN-LAST:event_jTextEditorMousePressed

    private void jMenuItem15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem15ActionPerformed
        // TODO add your handling code here:
        //seleccionar todo, menu emergente
        jTextEditor.selectAll();
    }//GEN-LAST:event_jMenuItem15ActionPerformed

    private void jMenuItem16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem16ActionPerformed
        // TODO add your handling code here:
        //seleccionar todo, copiar  menu emergente
        jTextEditor.copy();
    }//GEN-LAST:event_jMenuItem16ActionPerformed

    private void jMenuItem17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem17ActionPerformed
        // TODO add your handling code here:
        //seleccionar todo,pegar  menu emergente
        jTextEditor.paste();
        lh.leng(jTextEditor.getStyledDocument());
    }//GEN-LAST:event_jMenuItem17ActionPerformed

    private void jMenuItem18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem18ActionPerformed
        // TODO add your handling code here:
        //seleccionar todo,cortar menu emergente
        jTextEditor.cut();

    }//GEN-LAST:event_jMenuItem18ActionPerformed

    private void jMenuItem19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem19ActionPerformed
        // TODO add your handling code here:
        //busquedatexto
        jvBuscar.show();
    }//GEN-LAST:event_jMenuItem19ActionPerformed

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        // TODO add your handling code here:
        //ejecutar busqueda desde jdialog
        if (!jTextField1.getText().isEmpty()) {
            buscarT(jTextField1.getText());
        }
        jTextField1.setText("");

    }//GEN-LAST:event_jButton13ActionPerformed

    private void jMenuItem20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem20ActionPerformed
        // TODO add your handling code here:
        //buscar click derecho
        jvBuscar.show();
    }//GEN-LAST:event_jMenuItem20ActionPerformed

    private void jMenuItem21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem21ActionPerformed
        // TODO add your handling code here:
        //buscar y reemplazar barra
        jvBusRem.show();
    }//GEN-LAST:event_jMenuItem21ActionPerformed

    private void jButton15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton15ActionPerformed
        // TODO add your handling code here:
        if (!jTextField2.getText().isEmpty() && !jTextField3.getText().isEmpty()) {
            buscarTR(jTextField2.getText(), jTextField3.getText());
        } else {
            jTextPane5.setText("Faltan argumentos para buscar o reemplazar");
        }
        jTextField2.setText("");
        jTextField3.setText("");
    }//GEN-LAST:event_jButton15ActionPerformed

    private void jbCalcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbCalcActionPerformed
        // TODO add your handling code here:
        try {
            Process p = Runtime.getRuntime().exec("C:\\Windows\\System32\\calc.exe");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Lo siento, la función no disponible en este sistema.");
        }
    }//GEN-LAST:event_jbCalcActionPerformed

    private void jbTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbTxtActionPerformed
        // TODO add your handling code here:
        try {
            Process p = Runtime.getRuntime().exec("C:\\Windows\\System32\\notepad.exe");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            //e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Lo siento, la función no disponible en este sistema.");
        }
    }//GEN-LAST:event_jbTxtActionPerformed

    private void jMenuItem22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem22ActionPerformed
        // TODO add your handling code here:
        jTextEditor.setText(jTextEditor.getText().toLowerCase());
    }//GEN-LAST:event_jMenuItem22ActionPerformed

    private void jTextEditorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextEditorMouseClicked
        // TODO add your handling code here:

        jLabel6.setText("Linea/Caracter: " + String.valueOf((jTextEditor.getDocument().getDefaultRootElement()).getElementIndex(jTextEditor.getCaretPosition()) + 1)
                + " : " + String.valueOf(jTextEditor.getCaretPosition()));
        lh.leng(jTextEditor.getStyledDocument());
    }//GEN-LAST:event_jTextEditorMouseClicked

    private void jTextEditorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextEditorKeyTyped
        // TODO add your handling code here:

       jLabel6.setText("Linea/Caracter: " + String.valueOf((jTextEditor.getDocument().getDefaultRootElement()).getElementIndex(jTextEditor.getCaretPosition()) + 1) + " : " + String.valueOf(jTextEditor.getCaretPosition()));
       
    }//GEN-LAST:event_jTextEditorKeyTyped
   
    private void AcPlantilla() {
        jTextEditor.requestFocus();
        ejemPlantillas pl = new ejemPlantillas();
        jTextEditor.replaceSelection(pl.Plantilla(jLPlantilla.getSelectedIndex()));  
    }
    
    private void jLPlantillaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLPlantillaMousePressed
        // TODO add your handling code here: menu de plantillas predefinidas
        if (SwingUtilities.isRightMouseButton(evt)) {
            jMenuLP.show(jLPlantilla, evt.getX(), evt.getY());
        }
        //click derecho para agregar plantilla y con doble click
        if (evt.getClickCount() == 2) {
            AcPlantilla();
            lh.leng(jTextEditor.getStyledDocument());
        }
    }//GEN-LAST:event_jLPlantillaMousePressed

    private void jMenuItem23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem23ActionPerformed
        // TODO add your handling code here:
        //accion de la plantilla con menu click derecho
        AcPlantilla();
    }//GEN-LAST:event_jMenuItem23ActionPerformed

    private void jbAnalizaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbAnalizaActionPerformed
        // TODO add your handling code here:
        //boton que sirve para verificar errores sintacticos
        verSintax();
    }//GEN-LAST:event_jbAnalizaActionPerformed

    private void jMenuItem24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem24ActionPerformed
        // TODO add your handling code here:
        jTextNotRes.setText("");
    }//GEN-LAST:event_jMenuItem24ActionPerformed

    private void jMenuItem25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem25ActionPerformed
        // TODO add your handling code here:
        jvAyuda.show();
    }//GEN-LAST:event_jMenuItem25ActionPerformed

    private void jTree1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTree1MouseClicked
        // TODO add your handling code here:
        //menu de ayuda, evento donde si se da un clic se ve la vista previa del ejemplo, y dos clic lo insertan en el editor
        if (evt.getClickCount() == 1) {
            ejemplo = new ejemAyuda(jTree1.getRowForLocation(evt.getX(), evt.getY()),1);
            jTextPane7.setText(ejemplo.Ejemplos());
            jTextPane7.setCaretPosition(0);
            lh.leng(jTextPane7.getStyledDocument());
        }
        if (evt.getClickCount() == 2) {
            ejemplo = new ejemAyuda(jTree1.getRowForLocation(evt.getX(), evt.getY()),2);
            jTextEditor.replaceSelection(ejemplo.Ejemplos());
            jvAyuda.dispose();
        }
        lh.leng(jTextEditor.getStyledDocument());
        System.gc();
    }//GEN-LAST:event_jTree1MouseClicked

    private void jMenuItem26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem26ActionPerformed
        // TODO add your handling code here:
        //ampliar resultados
        jTextvResNot.setText(jTextNotRes.getText());
        jvResNot.show();
    }//GEN-LAST:event_jMenuItem26ActionPerformed

    private void jTextNotResMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextNotResMousePressed
        // TODO add your handling code here:
        if (SwingUtilities.isRightMouseButton(evt)) {
            jMenuC2.show(jTextNotRes, evt.getX(), evt.getY());

        }
    }//GEN-LAST:event_jTextNotResMousePressed

    private void jMenuItem27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem27ActionPerformed
        try {
            //autocompletar en menu clic izq
            Robot rob = new Robot();
            rob.keyPress(VK_CONTROL);
            rob.keyPress(VK_SPACE);
            rob.keyRelease(VK_CONTROL);
            rob.keyRelease(VK_SPACE);
        } catch (AWTException ex) {
            Logger.getLogger(Grafica.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_jMenuItem27ActionPerformed
    public void selectTema(String th){
        int seleccion=0;
        if(!jTextEditor.getText().isEmpty()){
           seleccion= JOptionPane.showConfirmDialog(null, "El editor se reiniciará, todo archivo no guardado se perderá.\n ¿Deseas proceder?", "Aviso",JOptionPane.OK_CANCEL_OPTION,JOptionPane.WARNING_MESSAGE);
        }
        
        if(seleccion ==0){
        switch(th){
            case "Blanco":
                this.dispose();
                try {
                    UIManager.setLookAndFeel(new FlatLightFlatIJTheme());
                } catch (UnsupportedLookAndFeelException ex) {
                    JOptionPane.showConfirmDialog(null,"Error al aplicar tema, por favor reinicia el programa","Error inesperado",JOptionPane.ERROR_MESSAGE);
                }
                new Grafica().setVisible(true);
                break;

            case "Gris":
                this.dispose();
                 try {
                     UIManager.setLookAndFeel(new FlatMaterialDarkerContrastIJTheme());
                 } catch (UnsupportedLookAndFeelException ex) {
                     JOptionPane.showConfirmDialog(null,"Error al aplicar tema, por favor reinicia el programa","Error inesperado",JOptionPane.ERROR_MESSAGE);
                  }
                  new Grafica().setVisible(true);
                break;

            case "GrisO":
                this.dispose();
                try {
                    UIManager.setLookAndFeel(new FlatVuesionIJTheme());
                } catch (UnsupportedLookAndFeelException ex) {
                    JOptionPane.showConfirmDialog(null,"Error al aplicar tema, por favor reinicia el programa","Error inesperado",JOptionPane.ERROR_MESSAGE);
                }
                new Grafica().setVisible(true);
                break;

            case "Sol":
                this.dispose();
                try {
                    UIManager.setLookAndFeel(new FlatSolarizedLightContrastIJTheme());
                } catch (UnsupportedLookAndFeelException ex) {
                   JOptionPane.showConfirmDialog(null,"Error al aplicar tema, por favor reinicia el programa","Error inesperado",JOptionPane.ERROR_MESSAGE);
                }
                new Grafica().setVisible(true);
                break;

            case "azul":
                this.dispose();
                try {
                    UIManager.setLookAndFeel(new FlatNightOwlContrastIJTheme());
                } catch (UnsupportedLookAndFeelException ex) {
                   JOptionPane.showConfirmDialog(null,"Error al aplicar tema, por favor reinicia el programa","Error inesperado",JOptionPane.ERROR_MESSAGE);
                }
                new Grafica().setVisible(true);
                break;                
            case "contraste":
                this.dispose();
                try {
                    UIManager.setLookAndFeel(new FlatHighContrastIJTheme());
                } catch (UnsupportedLookAndFeelException ex) {
                   JOptionPane.showConfirmDialog(null,"Error al aplicar tema, por favor reinicia el programa","Error inesperado",JOptionPane.ERROR_MESSAGE);
                }
                new Grafica().setVisible(true);
                break;                
        }
        
        }
        System.gc(); 
    }
    private void jMenuItem28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem28ActionPerformed
        // TODO add your handling code here:
        //tema gris oscuro
        selectTema("GrisO");
    }//GEN-LAST:event_jMenuItem28ActionPerformed

    private void jMenuItem29ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem29ActionPerformed
        // TODO add your handling code here:
        //tema solarizado
        selectTema("Sol");
    }//GEN-LAST:event_jMenuItem29ActionPerformed

    private void jMenuItem32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem32ActionPerformed
        // TODO add your handling code here:
        //personalizar colores
        jvColor.show();
    }//GEN-LAST:event_jMenuItem32ActionPerformed

    private void jButton18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton18ActionPerformed
        // TODO add your handling code here:
        //boton de seleccion de personalizacion de color
        switch (jComboBox1.getSelectedIndex()) {
            case 0:
                lp.setColor(jColorChooser1.getColor());
                break;
            case 1:
                jTextEditor.setBackground(jColorChooser1.getColor());
                jTextColors.setBackground(jColorChooser1.getColor());
                break;
            case 2:
                lh.setColorRes(jColorChooser1.getColor());
                try {
                    lh.leng(jTextEditor.getStyledDocument());
                    lh.leng(jTextColors.getStyledDocument());
                } catch (Exception e) {
                }

                break;
            case 3:
                lh.setColorStr(jColorChooser1.getColor());
                try {
                   lh.leng(jTextEditor.getStyledDocument());
                   lh.leng(jTextColors.getStyledDocument());
                } catch (Exception e) {
                }
                break;
            case 4:
                jTextEditor.setForeground(jColorChooser1.getColor());
                jTextColors.setForeground(jColorChooser1.getColor());
                break;
            case 5:
                lh.setColorCom(jColorChooser1.getColor());
                try {
                   lh.leng(jTextEditor.getStyledDocument());
                   lh.leng(jTextColors.getStyledDocument());
                } catch (Exception e) {
                }                
                break;

        }
        
       

    }//GEN-LAST:event_jButton18ActionPerformed

    private void jMenuItem30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem30ActionPerformed
        // TODO add your handling code here:
        ejecutarPr();
    }//GEN-LAST:event_jMenuItem30ActionPerformed

    private void jvFuenteWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_jvFuenteWindowOpened
        // TODO add your handling code here:
        //evento que se realiza al abrir la ventana chooser de fuente
        DefaultListModel le = new DefaultListModel();
        String fonts[]
                = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        for (String font : fonts) {
            le.addElement(font);
        }
        jLFuente.setModel(le);
        jLFuente.setSelectedIndex(0);
        jLEstilo.setSelectedIndex(0);
        jLTamanio.setSelectedIndex(3);
        /*elementos elegidos por defecto
        letra = jList2.getSelectedValue();
        estilo = Font.PLAIN;
        tLetra = Integer.parseInt(jList4.getSelectedValue());*/
    }//GEN-LAST:event_jvFuenteWindowOpened
    
    private void jLFuenteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLFuenteMouseClicked
        // TODO add your handling code here:
        //vista previa tipo de letra chooser de tipo de letra
        letra = jLFuente.getSelectedValue();
        jTextPane9.setFont(new Font(letra, estilo, tLetra));
    }//GEN-LAST:event_jLFuenteMouseClicked

    private void jLEstiloMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLEstiloMouseClicked
        // TODO add your handling code here:
        //vista previa del estilo de letra, chooser de tipo de letra

        String stilo = jLEstilo.getSelectedValue();
        switch (stilo) {
            case "Normal":
                estilo = Font.PLAIN;
                jTextPane9.setFont(new Font(letra, estilo, tLetra));
                break;
            case "Cursiva":
                estilo = Font.ITALIC;
                jTextPane9.setFont(new Font(letra, estilo, tLetra));
                break;
            case "Negrita":
                estilo = Font.BOLD;
                jTextPane9.setFont(new Font(letra, estilo, tLetra));
                break;
        }

    }//GEN-LAST:event_jLEstiloMouseClicked

    private void jLTamanioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLTamanioMouseClicked
        // TODO add your handling code here:
        //vista previa del tamaño de letra chooser de tipo letra
        tLetra = Integer.parseInt(jLTamanio.getSelectedValue());
        jTextPane9.setFont(new Font(letra, estilo, tLetra));
    }//GEN-LAST:event_jLTamanioMouseClicked

    private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton19ActionPerformed
        // TODO add your handling code here:
        //aceptar estilo de letra del chooser        
        switch (jComboBox2.getSelectedIndex()) {
            case 0:
                jTextEditor.setFont(new Font(letra, estilo, tLetra));
                break;
            case 1:
                jTextNotRes.setFont(new Font(letra, estilo, tLetra));
                break;
            case 2:
                jTextvResNot.setFont(new Font(letra, estilo, tLetra));
                break;
            case 3:
                jTextLVar.setFont(new Font(letra, estilo, tLetra));
                jTextvLVar.setFont(new Font(letra, estilo, tLetra));
                break;
        }
        jvFuente.dispose();
    }//GEN-LAST:event_jButton19ActionPerformed

    private void jMenuItem31ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem31ActionPerformed
        // TODO add your handling code here:
        //boton cambio de fuente chooser
        jvFuente.show();
    }//GEN-LAST:event_jMenuItem31ActionPerformed

    private void jButton22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton22ActionPerformed
        // TODO add your handling code here:
        //cerrar ventana de color chooser
        jvColor.dispose();
    }//GEN-LAST:event_jButton22ActionPerformed

    private void jButton20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton20ActionPerformed
        // TODO add your handling code here:
        //cerrrar ventana de chooser de fuente
        jvFuente.dispose();
    }//GEN-LAST:event_jButton20ActionPerformed

    private void jTextEditorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextEditorKeyReleased
        // TODO add your handling code here://el lexer del resaltado de texto refrezca en estos eventos:
    if(evt.getKeyCode() == KeyEvent.VK_SPACE || evt.getKeyCode() == KeyEvent.VK_ENTER || evt.getKeyCode() == KeyEvent.VK_TAB || evt.getKeyCode() == KeyEvent.VK_CONTROL || evt.getKeyCode() == KeyEvent.VK_BACK_SPACE ){
            lh.leng(jTextEditor.getStyledDocument());
        }
    }//GEN-LAST:event_jTextEditorKeyReleased

    private void jvColorWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_jvColorWindowOpened
        // TODO add your handling code here:
        //evento que actualiza el estado del muestrario de colores al abrir la ventana
        lh.leng(jTextColors.getStyledDocument());
    }//GEN-LAST:event_jvColorWindowOpened

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
         jvColor.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jMenuItem33ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem33ActionPerformed
        // TODO add your handling code here:.
        selectTema("azul");
    }//GEN-LAST:event_jMenuItem33ActionPerformed

    private void jMenuItem34ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem34ActionPerformed
        // TODO add your handling code here:
        selectTema("contraste");
    }//GEN-LAST:event_jMenuItem34ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
 /*
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Grafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Grafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Grafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Grafica.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }*/
        //</editor-fold>
        try {
            //FlatArcDarkIJTheme.setup();
            UIManager.setLookAndFeel(new FlatLightFlatIJTheme());
        } catch (UnsupportedLookAndFeelException ex) {
             JOptionPane.showConfirmDialog(null,"Error al aplicar tema, por favor reinicia el programa","Error inesperado",JOptionPane.ERROR_MESSAGE);
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

                new Grafica().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton15;
    private javax.swing.JButton jButton18;
    private javax.swing.JButton jButton19;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton20;
    private javax.swing.JButton jButton21;
    private javax.swing.JButton jButton22;
    private javax.swing.JColorChooser jColorChooser1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JList<String> jLEstilo;
    private javax.swing.JList<String> jLFuente;
    private javax.swing.JList<String> jLPlantilla;
    private javax.swing.JList<String> jLTamanio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPopupMenu jMenuC1;
    private javax.swing.JPopupMenu jMenuC2;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    private javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem17;
    private javax.swing.JMenuItem jMenuItem18;
    private javax.swing.JMenuItem jMenuItem19;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem20;
    private javax.swing.JMenuItem jMenuItem21;
    private javax.swing.JMenuItem jMenuItem22;
    private javax.swing.JMenuItem jMenuItem23;
    private javax.swing.JMenuItem jMenuItem24;
    private javax.swing.JMenuItem jMenuItem25;
    private javax.swing.JMenuItem jMenuItem26;
    private javax.swing.JMenuItem jMenuItem27;
    private javax.swing.JMenuItem jMenuItem28;
    private javax.swing.JMenuItem jMenuItem29;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem30;
    private javax.swing.JMenuItem jMenuItem31;
    private javax.swing.JMenuItem jMenuItem32;
    private javax.swing.JMenuItem jMenuItem33;
    private javax.swing.JMenuItem jMenuItem34;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPopupMenu jMenuLP;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JToolBar.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JSplitPane jSplitPane3;
    private javax.swing.JSplitPane jSplitPane4;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JTextPane jTextColors;
    private javax.swing.JTextPane jTextEditor;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextPane jTextLVar;
    private javax.swing.JTextPane jTextNotRes;
    private javax.swing.JTextPane jTextPane4;
    private javax.swing.JTextPane jTextPane5;
    private javax.swing.JTextPane jTextPane7;
    private javax.swing.JTextPane jTextPane9;
    private javax.swing.JTextPane jTextvLVar;
    private javax.swing.JTextPane jTextvResNot;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JTree jTree1;
    private javax.swing.JButton jbAbrirb;
    private javax.swing.JButton jbAnaliza;
    private javax.swing.JButton jbCalc;
    private javax.swing.JButton jbCopiar;
    private javax.swing.JButton jbCortar;
    private javax.swing.JButton jbDeshacer;
    private javax.swing.JButton jbEjecuta;
    private javax.swing.JButton jbGuardarb;
    private javax.swing.JButton jbLimpiar;
    private javax.swing.JButton jbPegar;
    private javax.swing.JButton jbRehacer;
    private javax.swing.JButton jbSelect;
    private javax.swing.JButton jbTxt;
    private javax.swing.JMenu jmArchivo;
    private javax.swing.JMenu jmAyuda;
    private javax.swing.JMenu jmConfig;
    private javax.swing.JMenu jmEdicion;
    private javax.swing.JDialog jvAyuda;
    private javax.swing.JDialog jvBusRem;
    private javax.swing.JDialog jvBuscar;
    private javax.swing.JDialog jvColor;
    private javax.swing.JDialog jvFuente;
    private javax.swing.JDialog jvResNot;
    private javax.swing.JDialog jvVariables;
    // End of variables declaration//GEN-END:variables

}
