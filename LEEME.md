
<div><h1>INTER-ES</h1>
<br>
<p>Inter-Es,  es un programa diseñado para el aprendizaje de la programación básica por medio del uso de pseudocódigo</p>
<p>A fecha septiembre de 2022 admite las siguientes operaciones: </p>
<ul>
<li>Manejo de variables tipo <i><b>NUMERICO/CADENA</b></i></li>
<li>Manejo parcial de variables de tipo <i><b>ENTERO</b></i> </li>
<li>Manejo de operaciones aritméticas básicas <i><b>(multiplicación, división, resta, suma, residual)</b></i></li>
<li>Estructura if-then-else <i><b>(else-if - case/switch pueden implementarse posteriormente)</b></i></li>
<li>Ingreso de datos por medio de ventana de diálogo. Ya sea con mensaje o sin este</li>
<li>Manejo de condiciones compuestas por medio de operadores <b>AND-OR</b></li>
<li>Estructura while </li>
<li>Estructura for / for each </li>
<li>Funciones matemáticas: <i><b>raíz cuadrada, raíz cúbica, número aleatorio, potenciación</b></i> </li>
<li>Conversión de tipos a: <i><b>CADENA, NUMERICO, ENTERO</b></i> </li>
</ul>
<h2>Adicionalmente la interfaz gráfica cuenta con las siguientes características. </h2>
<ul>
<li>Resaltado de sintaxis</li>
<li>Autocompletado de palabras reservadas por medio de combinación de teclas <i><b>CTRL+SPACE / TAB</b></i></li>
<li>Panel de variables</li>
<li>Selección de temas visuales </li>
<li>Configuración de estilos de fuente</li>
<li>Configuración de colores del entorno <i><b>(Cambio de colores de fondo, sintaxis, resaltador etc.)</b></i></li>
<li>Panel de ayuda con ejemplos ejecutables</li>
<li>Verificación de sintaxis </li>
<li> y varias herramientas básicas que son inherentes a cualquier editor de texto </li>
</ul>
</div>
